package com.thetechtonic.pb.brandportfolio.data.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thetechtonic.pb.brandportfolio.data.DataManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sid on 03-06-2018.
 */

public class RetrofitServiceFactory
{
    // TODO: 11/12/16 define your own base url

    private static final String BASE_URL   = "http://propcharge.com/PORTFOLIO_APP/portfolio_app/index.php/api/";
    private static final String ITC_BASE_URL   = "https://staging.psptracker.in/";

    private static final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static final Gson gson       = new GsonBuilder().setLenient()
            .registerTypeAdapterFactory(new DataTypeAdapterFactory()).create();

    private static final Retrofit.Builder     tBuilder   = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(
            GsonConverterFactory.create(gson));

    private static final Retrofit.Builder     itcBuilder   = new Retrofit.Builder().baseUrl(ITC_BASE_URL).addConverterFactory(
            GsonConverterFactory.create(gson));

    public static Retrofit sRetrofit,itcRetrofit;


    public static <S> S createInternalService (Class<S> serviceClass)
    {
        final String authHeader;
        if (DataManager.getUserToken() != null)
        {
            authHeader = "Bearer " + DataManager.getUserToken();
        }
        else
        {
            authHeader = "";
        }

        httpClient.addInterceptor(new ResponseInterceptor());
        //add authorization header
        httpClient.addInterceptor(chain -> {
            Request lOriginalRequest = chain.request();
            Request lRequest = lOriginalRequest.newBuilder().header("Authorization", authHeader)
                    .method(lOriginalRequest.method(), lOriginalRequest.body()).build();

            return chain.proceed(lRequest);
        });

        OkHttpClient lClient = httpClient.build();
        sRetrofit = tBuilder.client(lClient).build();
        return sRetrofit.create(serviceClass);
    }
    public static <S> S createITCService (Class<S> serviceClass)
    {
        final String authHeader;
        if (DataManager.getUserToken() != null)
        {
            authHeader = "Bearer " + DataManager.getUserToken();
        }
        else
        {
            authHeader = "";
        }

        httpClient.addInterceptor(new ResponseInterceptor());
        //add authorization header
        httpClient.addInterceptor(new BasicAuthInterceptor("portfolio", "P5CymnkJFuWQOw0"));

        OkHttpClient lClient = httpClient.build();
        itcRetrofit = itcBuilder.client(lClient).build();
        return itcRetrofit.create(serviceClass);
    }
}


