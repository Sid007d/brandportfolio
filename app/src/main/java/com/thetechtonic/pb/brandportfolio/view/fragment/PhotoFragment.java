package com.thetechtonic.pb.brandportfolio.view.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.data.event.BrandDetailsSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.SubBrandSuccessEvent;
import com.thetechtonic.pb.brandportfolio.models.PhotoList;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;
import com.thetechtonic.pb.brandportfolio.view.adapter.PhotoItemAdapter;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoFragment extends Fragment{


    private RecyclerView rcPhoto;
    private  LinearLayoutManager layoutManager;
    private List<PhotoList> photoDataModelList=new ArrayList<>();
    private PhotoItemAdapter photoItemAdapter;
    long startTime;
    private TextView tvNoData;
    private String isData;
    long endTime;
    SharedPreferences sharedPreferences;
    private long totalTime;
    private SharedPreferences.Editor editor;
    DBHelper dbHelper;
    private String subBrandId,variantId;

    public PhotoFragment() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Log.d("Method1","onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Method1","onCreate");
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_photo, container, false);
        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        dbHelper = new DBHelper(getActivity());

        initUI(view);
        inEvent();
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initUI(View view)
    {
    rcPhoto=view.findViewById(R.id.rcPhoto);
    tvNoData = view.findViewById(R.id.tvNoData);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void inEvent()
    {
        Bundle bundle=getArguments();
        isData = Objects.requireNonNull(bundle).getString("isData");
        if (Objects.requireNonNull(isData).equals("N")){
            tvNoData.setVisibility(View.VISIBLE);

        } else {
            tvNoData.setVisibility(View.GONE);

 //   photoDataModelList = Objects.requireNonNull(bundle).getParcelableArrayList("imageList");
    subBrandId = Objects.requireNonNull(bundle).getString("subBrandId");
            variantId = Objects.requireNonNull(bundle).getString("variantId");
    photoDataModelList =  dbHelper.getpackPhoto(subBrandId);

    if(photoDataModelList.isEmpty())
    {
        photoDataModelList = Objects.requireNonNull(bundle).getParcelableArrayList("imageList");
    }

    photoItemAdapter= new PhotoItemAdapter(getActivity(), photoDataModelList);
    layoutManager= new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
    rcPhoto.setLayoutManager(layoutManager);
    rcPhoto.setItemAnimator(new DefaultItemAnimator());
    rcPhoto.setAdapter(photoItemAdapter);
        }
}
    @Override
    public void onStart() {
        super.onStart();
        Log.d("Method1","onStart");
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onResume() {
        super.onResume();
        startTime = System.currentTimeMillis();
        Bundle bundle=getArguments();
        isData = Objects.requireNonNull(bundle).getString("isData");
        if (Objects.requireNonNull(isData).equals("N")){
            tvNoData.setVisibility(View.VISIBLE);

        } else {
            tvNoData.setVisibility(View.GONE);
            photoDataModelList = Objects.requireNonNull(bundle).getParcelableArrayList("imageList");
            subBrandId = Objects.requireNonNull(bundle).getString("subBrandId");
            photoItemAdapter= new PhotoItemAdapter(getActivity(), photoDataModelList);
            layoutManager= new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
            rcPhoto.setLayoutManager(layoutManager);
            rcPhoto.setItemAnimator(new DefaultItemAnimator());
            rcPhoto.setAdapter(photoItemAdapter);
        }
    }

    public static String msToString(long ms) {
        long totalSecs = ms/1000;
        long hours = (totalSecs / 3600);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00"
                : ((mins < 10)
                ? "0" + mins
                : "" + mins);
        String secsString = (secs == 0)
                ? "00"
                : ((secs < 10)
                ? "0" + secs
                : "" + secs);
        if (hours > 0)
            return hours+" "+"hr" + ":" + minsString+" "+"min" + ":" + secsString+" "+"sec";
        else if (mins > 0)
            return mins+" "+"min" + ":" + secsString+" "+"sec";
        else return secsString+" "+"sec";
    }


    @Override
    public void onPause() {
        super.onPause();
        TimeSpendModelDb timeSpendModelDbs;
        timeSpendModelDbs = dbHelper.getTImeList(subBrandId);
        totalTime = timeSpendModelDbs.getPhotoTime();
        endTime = System.currentTimeMillis();
        long timeSpend = endTime - startTime;
        SharedPreferenceManager.setPhotoTime(getActivity(), timeSpend+totalTime);
        dbHelper.updateTime(new TimeSpendModelDb(variantId, "", timeSpendModelDbs.getPackTime(), timeSpend+totalTime,timeSpendModelDbs.getVideoTime(),timeSpendModelDbs.getEngTime()));
//        editor = sharedPreferences.edit();
//        editor.putLong("DetailScreen2Time", timeSpend+totalTime).apply(); // Storing long
        Log.d("Fragment 1 Time:", msToString(timeSpend+totalTime));
       //Toast.makeText(getActivity(),  "Photo: "+msToString(timeSpend+totalTime), Toast.LENGTH_SHORT).show();
    }

}
