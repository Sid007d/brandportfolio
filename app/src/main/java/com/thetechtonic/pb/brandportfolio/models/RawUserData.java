package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RawUserData {

    @SerializedName("consumerId")
    @Expose
    private String consumerId;
    @SerializedName("activityId")
    @Expose
    private String activityId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("data")
    @Expose
    private List<RawTimeData> data;

    public RawUserData(String consumerId, String activityId, String date, List<RawTimeData> data) {
        this.consumerId = consumerId;
        this.activityId = activityId;
        this.date = date;
        this.data = data;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<RawTimeData> getData() {
        return data;
    }

    public void setData(List<RawTimeData> data) {
        this.data = data;
    }
}
