package com.thetechtonic.pb.brandportfolio;

import android.app.Application;
import android.content.Context;

import com.danikula.videocache.HttpProxyCacheServer;
import com.thetechtonic.pb.brandportfolio.data.remote.retrofit.MyAPIService;
import com.thetechtonic.pb.brandportfolio.data.remote.retrofit.RetrofitServiceFactory;


/**
 * Created by Sid on 03-06-2018.
 */

public class MyApplication extends Application
{
    private static MyApplication sApp;
    public MyAPIService tAPIService,itcAPIService;
    private HttpProxyCacheServer proxy;
    public static MyApplication getInstance ()
    {
        if (sApp == null)
        {
            sApp = new MyApplication();
        }

        return sApp;
    }

    @Override
    public void onCreate ()
    {
        super.onCreate();

        sApp = this;
        tAPIService = RetrofitServiceFactory.createInternalService(MyAPIService.class);
        itcAPIService = RetrofitServiceFactory.createITCService(MyAPIService.class);

    }
    public static HttpProxyCacheServer getProxy(Context context) {
        MyApplication app = (MyApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }

}