package com.thetechtonic.pb.brandportfolio.view.activity;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.data.DataManager;
import com.thetechtonic.pb.brandportfolio.data.event.BrandDetailsSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.ErrorEvent;
import com.thetechtonic.pb.brandportfolio.data.event.SubmitSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.TimeSpendSuccessEvent;
import com.thetechtonic.pb.brandportfolio.models.EngagementList;
import com.thetechtonic.pb.brandportfolio.models.FormattedTime;
import com.thetechtonic.pb.brandportfolio.models.OfflinePack;
import com.thetechtonic.pb.brandportfolio.models.PackList;
import com.thetechtonic.pb.brandportfolio.models.PhotoList;
import com.thetechtonic.pb.brandportfolio.models.RawTimeData;
import com.thetechtonic.pb.brandportfolio.models.RawUserData;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.models.VideoList;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;
import com.thetechtonic.pb.brandportfolio.utils.Utils;
import com.thetechtonic.pb.brandportfolio.utils.constants.S;
import com.thetechtonic.pb.brandportfolio.view.fragment.EngagementFragment;
import com.thetechtonic.pb.brandportfolio.view.fragment.PackFragment;
import com.thetechtonic.pb.brandportfolio.view.fragment.PhotoFragment;
import com.thetechtonic.pb.brandportfolio.view.fragment.VideoFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.thetechtonic.pb.brandportfolio.data.SessionManager.hideLoading;
import static com.thetechtonic.pb.brandportfolio.data.SessionManager.showLoading;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailActivity extends AppCompatActivity implements View.OnClickListener {


    private List<VideoList> videoDataList;
    private List<PhotoList> photoDataList;
    private List<EngagementList> engagementLists;
    private SharedPreferences sharedPreferences;
    private EventBus event;
    private String subBrandId, subBrandName, variantId;
    private TextView tvTimeSubmit;
    private TextView tvPack, tvPhoto, tvVideo, tvEngagement, tvSubBrandName;
    private Fragment fragment = null;
    private List<OfflinePack> offlinePacks;
    private View viewPack, viewPhoto, viewVideo, viewEng;
    private FragmentManager frgManager;
    private ImageView ivBack;
    private DBHelper dbHelper;
    private static ProgressDialog progressDialog;
    private PackList packList;
    private List<FormattedTime> formattedTimeList = new ArrayList<>();
    private List<RawTimeData> rawTimeDataList = new ArrayList<>();
    private String activityID,cousumerId;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        dbHelper = new DBHelper(this);
        progressDialog = new ProgressDialog(this);
        event = EventBus.getDefault();
        sharedPreferences = Objects.requireNonNull(getSharedPreferences(S.MyPREFERENCES, Context.MODE_PRIVATE));
        activityID  = sharedPreferences.getString(S.activityID,"");
        cousumerId = sharedPreferences.getString(S.ID,"");
        initUI();
        initEvent();
    }

    protected void initUI() {
        subBrandId = getIntent().getStringExtra("subBrandId");
        variantId = getIntent().getStringExtra("variantId");
        subBrandName = getIntent().getStringExtra("subBrandName");

        tvPack = findViewById(R.id.tvPack);
        tvPhoto = findViewById(R.id.tvPhoto);
        tvVideo = findViewById(R.id.tvVideo);
        tvEngagement = findViewById(R.id.tvEngagement);
        viewPack = findViewById(R.id.viewPack);
        viewPhoto = findViewById(R.id.viewPhoto);
        viewVideo = findViewById(R.id.viewVideo);
        viewEng = findViewById(R.id.viewEng);
        tvTimeSubmit = findViewById(R.id.tvTimeSubmit);
        ivBack = findViewById(R.id.ivBack);
        tvSubBrandName = findViewById(R.id.tvSubBrandName);


    }

    protected void initEvent() {
        videoDataList = new ArrayList<>();
        photoDataList = new ArrayList<>();
        offlinePacks = new ArrayList<>();
        engagementLists = new ArrayList<>();
        tvSubBrandName.setText(subBrandName);
        tvPack.setOnClickListener(this);
        tvPhoto.setOnClickListener(this);
        tvVideo.setOnClickListener(this);
        tvEngagement.setOnClickListener(this);

        tvTimeSubmit.setOnClickListener(view -> {
            showLoading("Please wait...");
            long getpackTime = SharedPreferenceManager.getPackTime(this);
            long getPhotoTime = SharedPreferenceManager.getPhotoTime(this);
            long getVideoTime = SharedPreferenceManager.getVideoTime(this);
            long getEngTime = SharedPreferenceManager.getEngTime(this);


            dbHelper.updateTime(new TimeSpendModelDb(variantId, "1", getpackTime, getPhotoTime, getVideoTime, getEngTime));

            String formattedPackTime = msToSeconds(getpackTime);
            String formattedPhotoTime = msToSeconds(getPhotoTime);
            String formattedVideoTime = msToSeconds(getVideoTime);
            String formattedEngageTime = msToSeconds(getEngTime);

            dbHelper.updateFormattedTime(new FormattedTime(variantId, "1", subBrandName, formattedPackTime, formattedPhotoTime, formattedVideoTime, formattedEngageTime));
            formattedTimeList = dbHelper.getFormattedTimeList();
            for (int i = 0;i<formattedTimeList.size();i++){
                String subBrandId = formattedTimeList.get(i).getSubBrandId();
                String packTime = formattedTimeList.get(i).getPackTime();
                String photoTime = formattedTimeList.get(i).getPhotoTime();
                String videoTime = formattedTimeList.get(i).getVideoTime();
                String engagementTime = formattedTimeList.get(i).getEngTime();
                RawTimeData rawTimeData = new RawTimeData(subBrandId,packTime,photoTime,videoTime,engagementTime);
                rawTimeDataList.add(rawTimeData);
            }
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c);

            RawUserData rawUserData = new RawUserData(cousumerId,activityID,formattedDate,rawTimeDataList);
            Gson gson = new Gson();
            String json = gson.toJson(rawUserData);
if(Utils.isConnectedToInternet(this))
{
    DataManager.submitRawData(rawUserData);
}
else
{
    try {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Info");
        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setButton("OK", (dialog, which) -> {
            alertDialog.dismiss();
            finish();
        });

        alertDialog.show();
    } catch (Exception e) {
        Log.d("TAG", "Show Dialog: " + e.getMessage());
    }
}

        });
        ivBack.setOnClickListener(view -> finish());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("DetailScreen1Time", 0);
        editor.putLong("DetailScreen2Time", 0);
        editor.putLong("DetailScreen3Time", 0);
        editor.putLong("DetailScreen4Time", 0);
        editor.apply();

        callBrandDetail();


    }


    public void callBrandDetail(){
        showLoading("Loading Data...");
        RequestBody userIdBody = RequestBody.create(MediaType.parse("text/plain"), subBrandId);
        DataManager.getSubBradDetails(userIdBody);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvPack:
                fragment = new PackFragment();
                frgManager = getSupportFragmentManager();
                viewPack.setVisibility(View.VISIBLE);
                viewPhoto.setVisibility(View.GONE);
                viewVideo.setVisibility(View.GONE);
                viewEng.setVisibility(View.GONE);
                if (packList == null) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putString("isData", "N");
                    fragment.setArguments(bundle3);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
                } else {
                    //            new DetailActivity.DownloadingTask().execute(String.valueOf(packList.getFileName()));
                    //offlinePacks = dbHelper.getsubBrandDetailsPackList(subBrandId);
                    Bundle bundle3 = new Bundle();
                    bundle3.putString("videoUrl", packList.getFileName());
                    bundle3.putString("subBrandId", subBrandId);
                    bundle3.putString("variantId", variantId);
                    bundle3.putString("subBrandName",subBrandName);
                    bundle3.putString("isData", "Y");
                    bundle3.putString("info", packList.getPackInfo());
                    fragment.setArguments(bundle3);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();

                }

                break;
            case R.id.tvPhoto:
                fragment = new PhotoFragment();
                frgManager = getSupportFragmentManager();
                viewPack.setVisibility(View.GONE);
                viewPhoto.setVisibility(View.VISIBLE);
                viewVideo.setVisibility(View.GONE);
                viewEng.setVisibility(View.GONE);
                if (photoDataList.isEmpty()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("isData", "N");
                    fragment.setArguments(bundle);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("imageList", (ArrayList<? extends Parcelable>) photoDataList);
                    bundle.putString("isData", "Y");
                    bundle.putString("subBrandId", subBrandId);
                    bundle.putString("variantId", variantId);
                    fragment.setArguments(bundle);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();

                }

                break;
            case R.id.tvVideo:
                fragment = new VideoFragment();
                frgManager = getSupportFragmentManager();
                viewPack.setVisibility(View.GONE);
                viewPhoto.setVisibility(View.GONE);
                viewVideo.setVisibility(View.VISIBLE);
                viewEng.setVisibility(View.GONE);
                if (videoDataList.isEmpty()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("isData", "N");
                    fragment.setArguments(bundle);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
                } else {
                    Bundle bundle1 = new Bundle();
                    bundle1.putParcelableArrayList("videoList", (ArrayList<? extends Parcelable>) videoDataList);
                    bundle1.putString("isData", "Y");
                    bundle1.putString("subBrandId", subBrandId);
                    bundle1.putString("variantId", variantId);
                    fragment.setArguments(bundle1);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
                }

                break;

            case R.id.tvEngagement:
                fragment = new EngagementFragment();
                frgManager = getSupportFragmentManager();
                viewPack.setVisibility(View.GONE);
                viewPhoto.setVisibility(View.GONE);
                viewVideo.setVisibility(View.GONE);
                viewEng.setVisibility(View.VISIBLE);
                if (engagementLists.isEmpty()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("isData", "N");
                    fragment.setArguments(bundle);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
                } else {
                    Bundle bundle2 = new Bundle();
                    bundle2.putParcelableArrayList("engageList", (ArrayList<? extends Parcelable>) engagementLists);
                    bundle2.putString("isData", "Y");
                    bundle2.putString("subBrandId", subBrandId);
                    bundle2.putString("variantId", variantId);
                    fragment.setArguments(bundle2);
                    frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
                }
                break;
        }
    }

    @Subscribe
    public void onSuccess(SubmitSuccessEvent event) {
        hideLoading();

        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("Message");
        dialog.setMessage("Data Submitted Successfully");
        dialog.setPositiveButton("OK", (dialog1, id) -> {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);

//            Intent intent = new Intent();
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//            finish();
            dialog1.dismiss();

        });
        final android.app.AlertDialog alert = dialog.create();
        alert.show();
        //  Toast.makeText(this, event.getResponseCode(), Toast.LENGTH_SHORT).show();

    }

    @Subscribe
    public void onSuccess (BrandDetailsSuccessEvent event)
    {
        hideLoading();
        videoDataList = new ArrayList<>();
        photoDataList = new ArrayList<>();
        engagementLists = new ArrayList<>();
        packList = new PackList();
        videoDataList = event.getVideoLists();
        photoDataList = event.getPhotoLists();
        engagementLists = event.getEngagementLists();
        packList = event.getPackList();

            fragment = new PackFragment();
        frgManager = getSupportFragmentManager();
        if (event.getPackList() == null) {
            Bundle bundle3 = new Bundle();
            bundle3.putString("isData", "N");
            fragment.setArguments(bundle3);
            frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();
        } else {
            //            new DetailActivity.DownloadingTask().execute(String.valueOf(packList.getFileName()));
            //offlinePacks = dbHelper.getsubBrandDetailsPackList(subBrandId);
            Bundle bundle3 = new Bundle();
            bundle3.putString("videoUrl", packList.getFileName());
            bundle3.putString("subBrandId", subBrandId);
            bundle3.putString("variantId", variantId);
            bundle3.putString("subBrandName",subBrandName);

            bundle3.putString("isData", "Y");
            bundle3.putString("info", packList.getPackInfo());
            fragment.setArguments(bundle3);
            frgManager.beginTransaction().replace(R.id.timecontainer, fragment).commit();

        }
    }

    @Subscribe
    public void onFailed(ErrorEvent event) {

        hideLoading();
        //       Toast.makeText(getActivity(), event.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onStart() {
        super.onStart();
        event.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        event.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();


        long getpackTime = SharedPreferenceManager.getPackTime(this);
        long getPhotoTime = SharedPreferenceManager.getPhotoTime(this);
        long getVideoTime = SharedPreferenceManager.getVideoTime(this);
        long getEngTime = SharedPreferenceManager.getEngTime(this);


        dbHelper.updateTime(new TimeSpendModelDb(variantId, "1", getpackTime, getPhotoTime, getVideoTime, getEngTime));

        String formattedPackTime = msToString(getpackTime);
        String formattedPhotoTime = msToString(getPhotoTime);
        String formattedVideoTime = msToString(getVideoTime);
        String formattedEngageTime = msToString(getEngTime);

        dbHelper.updateFormattedTime(new FormattedTime(variantId, "1", subBrandName, formattedPackTime, formattedPhotoTime, formattedVideoTime, formattedEngageTime));
    }

    public static String msToString(long ms) {
        long totalSecs = ms / 1000;
        long hours = (totalSecs / 3600);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00"
                : ((mins < 10)
                ? "0" + mins
                : "" + mins);
        String secsString = (secs == 0)
                ? "00"
                : ((secs < 10)
                ? "0" + secs
                : "" + secs);
        if (hours > 0)
            return hours + " " + "hr" + ":" + minsString + " " + "min" + ":" + secsString + " " + "sec";
        else if (mins > 0)
            return mins + " " + "min" + ":" + secsString + " " + "sec";
        else return secsString + " " + "sec";
    }

    public static void showLoading (String message)
    {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void hideLoading ()
    {
        if (progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }
    public static String msToSeconds(long ms) {
        long totalSecs = ms / 1000;
        return String.valueOf(totalSecs);
    }
}

