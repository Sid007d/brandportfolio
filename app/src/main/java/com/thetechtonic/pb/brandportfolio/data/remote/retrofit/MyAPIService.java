package com.thetechtonic.pb.brandportfolio.data.remote.retrofit;


import com.google.gson.JsonObject;
import com.thetechtonic.pb.brandportfolio.models.BrandDetailsResponse;
import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataResponse;
import com.thetechtonic.pb.brandportfolio.models.RawUserData;
import com.thetechtonic.pb.brandportfolio.models.SubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.SubmitDataResponse;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by Sid on 03-06-2018.
 */

public interface MyAPIService {

    //USER LOGIN
     @POST("brandList") //user SIGNUP
    public Call<BrandListResponse> getBranList();


    @Multipart
    @POST("subBrandList") //user SIGNUP
    public Call<SubBrandResponse> getSubBranList(@Part("brandId") RequestBody brandId);

    @Multipart
    @POST("subBrandDetails") //getSubBradDetails  callTime
    public Call<BrandDetailsResponse> getSubBradDetails(@Part("subBrandId") RequestBody brandId);

    @Multipart
    @POST("timeSpent") //getSubBradDetails  getofflinedata
    public Call<TimeSpendResponse> callTime(@Part("customerId") RequestBody customerId, @Part("customerName") RequestBody customerName,@Part("subBrandId") RequestBody brandId,@Part("subBrandPackTime") RequestBody subBrandPackTime, @Part("subBrandPhotoTime") RequestBody subBrandPhotoTime, @Part("subBrandVideoTime") RequestBody subBrandVideoTime, @Part("subBrandEngagementTime") RequestBody subBrandEngagementTime);

    @Multipart
    @POST("allDetails") //getofflinedata
    public Call<OfflineDataResponse> getofflinedata(@Part("deviceId") RequestBody deviceId);
    @Multipart
    @POST("allDetails_v2") //getofflinedata
    public Call<OfflineDataResponse> getofflinedataV2(@Part("deviceId") RequestBody deviceId);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("engagement_stats") //getofflinedata
    public Call<SubmitDataResponse> submitRawData(@Body RawUserData rawUserData);

}
