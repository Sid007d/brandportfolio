package com.thetechtonic.pb.brandportfolio.view.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataList;
import com.thetechtonic.pb.brandportfolio.utils.Utils;
import com.thetechtonic.pb.brandportfolio.view.fragment.FragmentWithVideo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public  class MyPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS;
    private Context mContext;
    private String downloadUrl = "", downloadFileName = "";
    List<OfflineDataList> brandLists=new ArrayList<>();

    public MyPagerAdapter(Context context,FragmentManager fragmentManager,List<OfflineDataList> brandLists, int noOfItems) {
        super(fragmentManager);
        this.mContext = context;
        this.brandLists=brandLists;
        NUM_ITEMS = noOfItems;
    }

    // Returns total number of pages.
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for a particular page.
    @Override
    public Fragment getItem(int position) {
        OfflineDataList branditem = brandLists.get(position);

                return FragmentWithVideo.newInstance(branditem.getBrandName(),branditem.getBrandVideo(),branditem.getBrandId());


    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Tab " + position;
    }

}
