package com.thetechtonic.pb.brandportfolio.models;

import java.io.File;

/**
 * Created by admin on 9/6/2018.
 */

public class DataModels {
    String id;
    String brandId;
    String brandName;
    String  video;

    public DataModels(String brandId,String brandName,String video) {
        this.brandName = brandName;
        this.brandId=brandId;
        this.video=video;
    }
    public DataModels()
    {

    }
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
