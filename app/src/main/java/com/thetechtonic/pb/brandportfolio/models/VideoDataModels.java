package com.thetechtonic.pb.brandportfolio.models;

/**
 * Created by admin on 8/29/2018.
 */

public class VideoDataModels {

    int fileName;
    String title;

    public VideoDataModels(int fileName,String title) {
        this.fileName = fileName;
        this.title=title;
    }

    public int getFileName() {
        return fileName;
    }

    public void setFileName(int fileName) {
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
