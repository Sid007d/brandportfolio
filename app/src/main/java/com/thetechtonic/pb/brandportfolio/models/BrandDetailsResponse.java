package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrandDetailsResponse {

    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("pack")
    @Expose
    private PackList pack;
    @SerializedName("photo")
    @Expose
    private List<PhotoList> photo = null;
    @SerializedName("video")
    @Expose
    private List<VideoList> video = null;
    @SerializedName("engagement")
    @Expose
    private List<EngagementList> engagement = null;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public PackList getPack() {
        return pack;
    }

    public void setPack(PackList pack) {
        this.pack = pack;
    }

    public List<PhotoList> getPhoto() {
        return photo;
    }

    public void setPhoto(List<PhotoList> photo) {
        this.photo = photo;
    }

    public List<VideoList> getVideo() {
        return video;
    }

    public void setVideo(List<VideoList> video) {
        this.video = video;
    }

    public List<EngagementList> getEngagement() {
        return engagement;
    }

    public void setEngagement(List<EngagementList> engagement) {
        this.engagement = engagement;
    }

}
