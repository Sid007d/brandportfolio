package com.thetechtonic.pb.brandportfolio.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.view.fragment.EngagementFragment;
import com.thetechtonic.pb.brandportfolio.view.fragment.PackFragment;
import com.thetechtonic.pb.brandportfolio.view.fragment.PhotoFragment;
import com.thetechtonic.pb.brandportfolio.view.fragment.VideoFragment;

/**
 * Created by Sid on 28-08-2018.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new PackFragment();
        } else if (position == 1){
            return new PhotoFragment();
        } else if (position == 2){
            return new VideoFragment();
        } else {
            return new EngagementFragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 4;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "PACK";
            case 1:
                return "PHOTOS";
            case 2:
                return "VIDEOS";
            case 3:
                return "ENGAGEMENT";
            default:
                return null;
        }
    }

}
