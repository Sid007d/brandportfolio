package com.thetechtonic.pb.brandportfolio.models;

public class USERIDMODELS {

    String userId;

    public USERIDMODELS(String userId) {
        this.userId = userId;
    }

    public USERIDMODELS() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
