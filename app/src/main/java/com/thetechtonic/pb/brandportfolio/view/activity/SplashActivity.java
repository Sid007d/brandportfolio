package com.thetechtonic.pb.brandportfolio.view.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.models.USERIDMODELS;
import com.thetechtonic.pb.brandportfolio.utils.constants.S;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class SplashActivity extends AppCompatActivity {

    VideoView videoView;
    DBHelper dbHelper;
    private static final int    PERMISSIONS_REQUEST = 1234;
    private List<USERIDMODELS> useridmodelsList;
    private List<String> userIdString;
    String otp;


    public String[] getRequiredPermissions() {
        String[] permissions = null;
        try {
            permissions = getPackageManager().getPackageInfo(getPackageName(),
                    PackageManager.GET_PERMISSIONS).requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (permissions == null) {
            return new String[0];
        } else {
            return permissions.clone();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        dbHelper=new DBHelper(this);
        videoView =  findViewById(R.id.videoView);
        displayMessage(getIntent());

        DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        }
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.psp_splash);
        videoView.setVideoURI(video);

        videoView.setOnCompletionListener(mp -> startNextActivity());

        videoView.start();


    }

    private void startNextActivity() {
        if (isFinishing())
            return;
        Intent intent = new Intent(this,OtpVerifyActivity.class);
        intent.putExtra("otp",otp);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        displayMessage(intent);
    }

    /**
     * Displays the received message in a dialog.
     */
    private void displayMessage(Intent intent) {
        Bundle extras = intent.getExtras();
        useridmodelsList = new ArrayList<>();
        userIdString = new ArrayList<>();
        useridmodelsList = dbHelper.getuserId();

        if (extras != null) {
            String consumerID = extras.getString("consumerID");
            String activityID = extras.getString("activityID");
            otp = extras.getString("otp");
            String contact_number = extras.getString("contact_number");
            String Date = extras.getString("date");
            SharedPreferences preferences = getSharedPreferences(S.MyPREFERENCES, Context.MODE_PRIVATE);
            preferences.edit().putString(S.ID, consumerID).apply();
            preferences.edit().putString(S.OTP, otp).apply();
            preferences.edit().putString(S.activityID, activityID).apply();
            Toast.makeText(this,activityID+" "+consumerID,Toast.LENGTH_SHORT).show();
            if (useridmodelsList.isEmpty()){
                dbHelper.insertUserId(new USERIDMODELS(activityID));
            }else {
                for (int k=0;k<useridmodelsList.size();k++){
                    userIdString.add(useridmodelsList.get(k).getUserId());
                    }
                    if (!userIdString.contains(activityID)){
                            dbHelper.insertUserId(new USERIDMODELS(activityID));
                        }else {
                            Toast.makeText(this,"User Already Exist",Toast.LENGTH_SHORT).show();
                        }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions() {
        String[] ungrantedPermissions = requiredPermissionsStillNeeded();
        if (ungrantedPermissions.length == 0) {

        } else {
            requestPermissions(ungrantedPermissions, PERMISSIONS_REQUEST);
        }
    }

    /**
     * Convert the array of required permissions to a {@link Set} to remove
     * redundant elements. Then remove already granted permissions, and return
     * an array of ungranted permissions.
     */
    @TargetApi(23)
    private String[] requiredPermissionsStillNeeded() {

        Set<String> permissions = new HashSet<String>();
        permissions.addAll(Arrays.asList(getRequiredPermissions()));
        for (Iterator<String> i = permissions.iterator(); i.hasNext();) {
            String permission = i.next();
            if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
                Log.d(SplashActivity.class.getSimpleName(),
                        "Permission: " + permission + " already granted.");
                i.remove();
            } else {
                Log.d(SplashActivity.class.getSimpleName(),
                        "Permission: " + permission + " not yet granted.");
            }
        }
        return permissions.toArray(new String[permissions.size()]);
    }

}
