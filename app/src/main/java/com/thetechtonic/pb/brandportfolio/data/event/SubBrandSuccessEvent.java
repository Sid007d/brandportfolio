package com.thetechtonic.pb.brandportfolio.data.event;

import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.SubBrandResponse;

import java.util.ArrayList;
import java.util.List;

public class SubBrandSuccessEvent extends BaseEvent {

    String message;
    String responseCode;
    List<SubBrandResponse.SubBrandList> brandList=new ArrayList<>();


    public SubBrandSuccessEvent(SubBrandResponse brandListResponse) {

        message=brandListResponse.getResponseMessage();
        responseCode=brandListResponse.getResponseCode();
        brandList=brandListResponse.getData();


    }

    public List<SubBrandResponse.SubBrandList> getBrandList() {
        return brandList;
    }

    public String getMessage() {
        return message;
    }

    public String getResponseCode() {
        return responseCode;
    }
}
