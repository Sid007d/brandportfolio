package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrandListResponse {

    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("data")
    @Expose
    private List<BrandList> data = null;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<BrandList> getData() {
        return data;
    }

    public void setData(List<BrandList> data) {
        this.data = data;
    }

    public static class BrandList {

        @SerializedName("brandId")
        @Expose
        private String brandId;
        @SerializedName("brandName")
        @Expose
        private String brandName;
        @SerializedName("brandVideo")
        @Expose
        private String brandVideo;

        public BrandList(String brandId, String brandName, String brandVideo) {
            this.brandId = brandId;
            this.brandName = brandName;
            this.brandVideo = brandVideo;
        }

        public BrandList() {
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getBrandVideo() {
            return brandVideo;
        }

        public void setBrandVideo(String brandVideo) {
            this.brandVideo = brandVideo;
        }
    }
}
