package com.thetechtonic.pb.brandportfolio.view.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;

import java.util.Locale;


public class LocaleActivity extends AppCompatActivity {

    private TextView tvEnglish,tvHindi,tvBangla,tvSelectLanguage,tvAssamese,tvBodo,tvGujrati,tvKannada,tvMalayalam,tvOriya,
    tvMarathi,tvTamil,tvTelugu,tvUrdu;
    private Button btnProceed;
    private int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locale);
        initUI();
        initEvent();
    }

    public void initUI(){
        tvEnglish = findViewById(R.id.tvEnglish);
        tvHindi = findViewById(R.id.tvHindi);
        tvBangla = findViewById(R.id.tvBangla);
        tvAssamese = findViewById(R.id.tvAssamese);
        tvBodo = findViewById(R.id.tvBoro);
        tvGujrati = findViewById(R.id.tvGujrati);
        tvKannada = findViewById(R.id.tvKannada);
        tvMalayalam = findViewById(R.id.tvMalayalam);
        tvOriya = findViewById(R.id.tvOdia);
        tvMarathi = findViewById(R.id.tvBMarathi);
        tvTamil = findViewById(R.id.tvTamil);
        tvTelugu = findViewById(R.id.tvTelugu);
        tvUrdu = findViewById(R.id.tvUrdu);
        tvSelectLanguage = findViewById(R.id.tvSelectLanguage);
        btnProceed = findViewById(R.id.btnProceed);
    }

    public void initEvent(){

        //ENGLISH
        tvEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                SharedPreferenceManager.setLanguage(LocaleActivity.this,"en");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //HINDI
        tvHindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("hi");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"hi");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //BENGALI
        tvBangla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("bn");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                SharedPreferenceManager.setLanguage(LocaleActivity.this,"bn");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //ASSAMESE
        tvAssamese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("as");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"as");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //BODO
        tvBodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("brx");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"brx");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Gujurati
        tvGujrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("gu");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"gu");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Kannada
        tvKannada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("kn");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"kn");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Malayalam
        tvMalayalam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("ml");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"ml");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Marathi
        tvMarathi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("mr");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"mr");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Odia
        tvOriya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("or");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"or");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Tamil
        tvTamil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("ta");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"ta");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Telugu
        tvTelugu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("te");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"te");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });

        //Urdu
        tvUrdu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Locale locale = new Locale("ur");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                tvEnglish.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvHindi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBangla.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvAssamese.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvBodo.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvGujrati.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMalayalam.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvMarathi.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvOriya.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvKannada.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTamil.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvTelugu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.unselected_border));
                tvUrdu.setBackground(ContextCompat.getDrawable(LocaleActivity.this, R.drawable.selected_layout_border));

                SharedPreferenceManager.setLanguage(LocaleActivity.this,"ur");
                tvSelectLanguage.setText(getResources().getString(R.string.select_language));
                btnProceed.setText(getResources().getString(R.string.submit));
                //Toast.makeText(LocaleActivity.this, getResources().getString(R.string.select_language), Toast.LENGTH_SHORT).show();
            }
        });
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == 0){
                    Toast.makeText(LocaleActivity.this,"Please Select any language",Toast.LENGTH_SHORT).show();
                } else{
                    startActivity(new Intent(LocaleActivity.this, MainActivity.class));
                }

            }
        });
    }
}
