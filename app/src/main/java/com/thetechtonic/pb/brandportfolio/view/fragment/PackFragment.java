package com.thetechtonic.pb.brandportfolio.view.fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;


import android.os.Environment;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.models.OfflinePack;
import com.thetechtonic.pb.brandportfolio.models.PackList;
import com.thetechtonic.pb.brandportfolio.models.Packvideo;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.utils.CheckForSDCard;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;
import com.thetechtonic.pb.brandportfolio.utils.Utils;
import com.thetechtonic.pb.brandportfolio.utils.constants.S;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;



/**
 * A simple {@link Fragment} subclass.
 */
public class PackFragment extends Fragment {

    long startTime;
    long endTime;
    SharedPreferences sharedPreferences;
    private long totalTime;
    private SharedPreferences.Editor editor;
    private Uri videoUri;
    private TextView tvNoData;
    private String isData;
    VideoView videoView;
    private RelativeLayout container;
    private String videoUrl;
    private String info;
    private ImageView tvInfo;
    public static final int progress_bar_type = 0;
    private String downloadFileName = "";
    int flag;
    private ProgressDialog mProgressDialog3;
    DBHelper dbHelper;
    private String subBrandId, variantId;
    List<OfflinePack> offlinePackList;
    private List<String> fileList;
    String checkurl;
    File imgFile;
    private ImageView ivWarning;
    private String subBrandName;

    public PackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_pack, container, false);
        dbHelper=new DBHelper(getActivity());
        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(S.MyPREFERENCES, Context.MODE_PRIVATE);
        // Initialize the progress dialog
        mProgressDialog3 = new ProgressDialog(getActivity());
        mProgressDialog3.setIndeterminate(false);
        mProgressDialog3.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog3.setTitle("Downloading Pack Videos");
        mProgressDialog3.setMessage("Please wait, we are downloading your files...");
        mProgressDialog3.setCancelable(false);
        initUI(view);
        inEvent();
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initUI(View view)
    {
        videoView = view.findViewById(R.id.videoViewFrag);
        tvNoData = view.findViewById(R.id.tvNoData);
        tvInfo=view.findViewById(R.id.tvInfo);
        ivWarning = view.findViewById(R.id.ivWarninng);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void inEvent() {
        String language = SharedPreferenceManager.getLanguage(getActivity());
        if (language.equals("en")){
            ivWarning.setImageResource(R.mipmap.hindi);
        }else if (language.equals("hi")){
            ivWarning.setImageResource(R.mipmap.hindi);
        } else if (language.equals("bn")){
            ivWarning.setImageResource(R.mipmap.bengali);
        } else if (language.equals("as")){
            ivWarning.setImageResource(R.mipmap.assamese);
        } else if (language.equals("brx")){
            ivWarning.setImageResource(R.mipmap.bodo);
        } else if (language.equals("gu")){
            ivWarning.setImageResource(R.mipmap.gujrati);
        } else if (language.equals("kn")){
            ivWarning.setImageResource(R.mipmap.kannada);
        } else if (language.equals("ml")){
            ivWarning.setImageResource(R.mipmap.malyalam);
        } else if (language.equals("mr")){
            ivWarning.setImageResource(R.mipmap.marathi);
        } else if (language.equals("or")){
            ivWarning.setImageResource(R.mipmap.oriya);
        } else if (language.equals("ta")){
            ivWarning.setImageResource(R.mipmap.tamil);
        } else if (language.equals("te")){
            ivWarning.setImageResource(R.mipmap.telugu);
        } else if (language.equals("ur")){
            ivWarning.setImageResource(R.mipmap.urdu);
        }

        Bundle bundle=getArguments();
        isData = Objects.requireNonNull(bundle).getString("isData");
        if (Objects.requireNonNull(isData).equals("N")){
            tvNoData.setVisibility(View.VISIBLE);

        } else  {
            tvNoData.setVisibility(View.GONE);
            videoUrl = Objects.requireNonNull(bundle).getString("videoUrl");
            subBrandId = Objects.requireNonNull(bundle).getString("subBrandId");
            variantId = Objects.requireNonNull(bundle).getString("variantId");
            info = Objects.requireNonNull(bundle).getString("info");
            subBrandName = Objects.requireNonNull(bundle).getString("subBrandName");
            String path = videoUrl.replace(Utils.packUrl, "");
            String basePath = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + Utils.downloadSubDirectory;

            offlinePackList = dbHelper.getsubBrandDetailsPackList(subBrandId);
            if (offlinePackList.isEmpty()){
                dbHelper.insertSubBrandDetailsPack(new OfflinePack(subBrandId, basePath + "/" + path));
                new PackFragment.PackVideoDownloadingTask().execute(videoUrl);
            } else {
                fileList = new ArrayList<>();
                for (int i=0;i<offlinePackList.size();i++){
                    String searchUrl = offlinePackList.get(i).getFileName().replace(basePath,"");
                    fileList.add(searchUrl);
                }

                if (fileList.contains("/"+path)){
                    videoUri = Uri.parse(basePath + "/" + path);
                    videoView.setVideoPath(basePath + "/" + path);
                    videoView.requestFocus();
                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.setLooping(true);
                        }
                    });
                    videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.d("API123", "What " + what + " extra " + extra);
                            return false;
                        }
                    });
                    videoView.start();
                }
            }
        }
        tvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_dialog);
                TextView tvNODATAInfo = (TextView) dialog.findViewById(R.id.tvNODATAInfo);
                TextView tvInfo = (TextView) dialog.findViewById(R.id.tvInfo);
                TextView tvBrandName = dialog.findViewById(R.id.tvBrandName);

                if(info==null || info.equals(""))
                {

                    tvNODATAInfo.setVisibility(View.VISIBLE);
                    tvInfo.setVisibility(View.GONE);
                }
                else
                {
                    tvInfo.setText(info);
                    tvBrandName.setText(subBrandName);
                    tvNODATAInfo.setVisibility(View.GONE);
                    tvInfo.setVisibility(View.VISIBLE);
                }

                ImageView btnIvClose = (ImageView) dialog.findViewById(R.id.btnIvClose);
                btnIvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });
    }

    public static String msToString(long ms) {
        long totalSecs = ms/1000;
        long hours = (totalSecs / 3600);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00"
                : ((mins < 10)
                ? "0" + mins
                : "" + mins);
        String secsString = (secs == 0)
                ? "00"
                : ((secs < 10)
                ? "0" + secs
                : "" + secs);
        if (hours > 0)
            return hours+" "+"hr" + ":" + minsString+" "+"min" + ":" + secsString+" "+"sec";
        else if (mins > 0)
            return mins+" "+"min" + ":" + secsString+" "+"sec";
        else return secsString+" "+"sec";
    }

    @Override
    public void onResume() {
        super.onResume();
        startTime = System.currentTimeMillis();
    }

    @Override
    public void onPause() {
        super.onPause();
        TimeSpendModelDb timeSpendModelDbs;
        timeSpendModelDbs = dbHelper.getTImeList(subBrandId);
        totalTime = timeSpendModelDbs.getPackTime();
        endTime = System.currentTimeMillis();
        long timeSpend = endTime - startTime;
        SharedPreferenceManager.setPackTime(getActivity(), timeSpend+totalTime);
        dbHelper.updateTime(new TimeSpendModelDb(variantId,"1" ,timeSpend+totalTime, timeSpendModelDbs.getPhotoTime(),timeSpendModelDbs.getVideoTime(),timeSpendModelDbs.getEngTime()));
//        editor = sharedPreferences.edit();
//        editor.putLong("DetailScreen1Time", timeSpend+totalTime).apply(); // Storing long
        Log.d("Fragment 1 Time:", msToString(timeSpend+totalTime));
        //Toast.makeText(getActivity(),"Pack: "+msToString(timeSpend+totalTime),Toast.LENGTH_SHORT).show();
    }

    private class PackVideoDownloadingTask extends AsyncTask<String, String, String> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog3.setMax(1);
            mProgressDialog3.show();
        }

        @Override
        protected String doInBackground(String... arg0) {

                try {
                    downloadFileName = arg0[0].replace(Utils.packUrl, "");
                    URL url = new URL(arg0[0]);//Create Download URl
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                    c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                    c.connect();//connect the URL Connection
                    int lenghtOfFile = c.getContentLength();
                    //If Connection response is not OK then show Logs
                    if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.e("TAG", "Server returned HTTP " + c.getResponseCode()
                                + " " + c.getResponseMessage());

                    }


                    //Get File if SD card is present
                    if (new CheckForSDCard().isSDCardPresent()) {

                        apkStorage = new File(Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + Utils.downloadSubDirectory);
                    } else
                        Toast.makeText(getActivity(), "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                    //If File is not present create directory
                    if (!apkStorage.exists()) {
                        apkStorage.mkdir();
                        Log.e("TAG", "Directory Created.");
                    }

                    outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                    //Create New File if not present
                    if (!outputFile.exists()) {
                        outputFile.createNewFile();
                        Log.e("TAG", "File Created");
                    }

                    FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                    InputStream is = c.getInputStream();//Get InputStream for connection

                    byte[] buffer = new byte[1024];//Set buffer type
                    int len1 = 0;//init length
                    long total = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        total += len1;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                        fos.write(buffer, 0, len1);//Write new file
                    }

                    //Close all connection after doing task
                    fos.close();
                    is.close();

                } catch (Exception e) {

                    //Read exception if something went wrong
                    e.printStackTrace();
                    outputFile = null;
                    Log.e("TAG", "Download Error Exception " + e.getMessage());
                }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {


        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (outputFile!=null) {
                mProgressDialog3.dismiss();
                offlinePackList = dbHelper.getsubBrandDetailsPackList(subBrandId);
                String path = videoUrl.replace(Utils.packUrl, "");
                String basePath = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + Utils.downloadSubDirectory;

                fileList = new ArrayList<>();
                for (int i=0;i<offlinePackList.size();i++){
                    String searchUrl = offlinePackList.get(i).getFileName().replace(basePath,"");
                    fileList.add(searchUrl);
                }

                if (fileList.contains("/"+path)){
                    videoUri = Uri.parse(basePath + "/" + path);
                    videoView.setVideoPath(basePath + "/" + path);
                    videoView.requestFocus();
                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.setLooping(true);
                        }
                    });
                    videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.d("API123", "What " + what + " extra " + extra);
                            return false;
                        }
                    });
                    videoView.start();
                }

            }
        }
    }

}
