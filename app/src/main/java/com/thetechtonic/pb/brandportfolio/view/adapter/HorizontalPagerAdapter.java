package com.thetechtonic.pb.brandportfolio.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gigamole.infinitecycleviewpager.VerticalInfiniteCycleViewPager;
import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.models.OfflineSubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.SubBrandResponse;
import com.thetechtonic.pb.brandportfolio.utils.CommonHelper;
import com.thetechtonic.pb.brandportfolio.view.activity.DetailActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by GIGAMOLE on 7/27/16.
 */
public class HorizontalPagerAdapter extends PagerAdapter {

    private List<OfflineSubBrandResponse> subBrandLists = new ArrayList<>();
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private boolean mIsTwoWay;

    public HorizontalPagerAdapter(final Context context, final boolean isTwoWay, List<OfflineSubBrandResponse> subBrandLists1) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mIsTwoWay = isTwoWay;
        subBrandLists = subBrandLists1;
    }

    @Override
    public int getCount() {
        return subBrandLists.size();
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;


            view = mLayoutInflater.inflate(R.layout.item, container, false);
            setImage(view, subBrandLists.get(position).getSubBrandPhoto());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,DetailActivity.class);
                intent.putExtra("subBrandId",subBrandLists.get(position).getSubBrandId());
                intent.putExtra("subBrandName",subBrandLists.get(position).getSubBrandName());
                mContext.startActivity(intent);
            }
        });

        container.addView(view);
        return view;
    }

    public  void setupItem(final View view, final OfflineSubBrandResponse subBrandList) {
        final TextView txt = (TextView) view.findViewById(R.id.txt_item);
        txt.setText(subBrandList.getSubBrandName());

        final ImageView img = (ImageView) view.findViewById(R.id.img_item);

        Glide.with(mContext).load(subBrandList.getSubBrandPhoto()).into(img);
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }

    private void setImage(View view,String imgPath)
    {

        try {
            File f=new File(imgPath);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            ImageView img=(ImageView)view.findViewById(R.id.img_item);
            img.setImageBitmap(b);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }


}
