package com.thetechtonic.pb.brandportfolio.view.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.models.VideoDataModels;
import com.thetechtonic.pb.brandportfolio.models.VideoList;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;
import com.thetechtonic.pb.brandportfolio.view.adapter.VideoItemAdapter;
import com.thetechtonic.pb.brandportfolio.view.interfaces.FragmentLifecycle;

import java.util.ArrayList;
import java.util.List;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {

     private RecyclerView rcVideo;
     private  LinearLayoutManager layoutManager;
     List<VideoList> videoDataModelsList=new ArrayList<>();
     VideoItemAdapter videoItemAdapter;
    private TextView tvNoData;
    private String isData;
    long startTime;
    long endTime;
    SharedPreferences sharedPreferences;
    private long totalTime;
    private SharedPreferences.Editor editor;
    DBHelper dbHelper;
    private String subBrandId,variantId;
    public VideoFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_video, container, false);
        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        dbHelper = new DBHelper(getActivity());
        Bundle bundle=getArguments();
        videoDataModelsList = Objects.requireNonNull(bundle).getParcelableArrayList("videoList");
        initUI(view);
        inEvent();
        return view;

    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initUI(View view)
    {
    //    videodata();
        rcVideo=view.findViewById(R.id.rcVideo);
        tvNoData = view.findViewById(R.id.tvNoData);

    }


    public void inEvent() {
        Bundle bundle=getArguments();
        isData = Objects.requireNonNull(bundle).getString("isData");
        if (Objects.requireNonNull(isData).equals("N")){
            tvNoData.setVisibility(View.VISIBLE);

        } else {
            tvNoData.setVisibility(View.GONE);

            subBrandId = Objects.requireNonNull(bundle).getString("subBrandId");
            variantId = Objects.requireNonNull(bundle).getString("variantId");
           videoDataModelsList=dbHelper.getpackVideo(subBrandId);

           if(videoDataModelsList.isEmpty())
           {
               videoDataModelsList = Objects.requireNonNull(bundle).getParcelableArrayList("videoList");
           }

        layoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        rcVideo.setLayoutManager(layoutManager);
        rcVideo.setItemAnimator(new DefaultItemAnimator());
        videoItemAdapter= new VideoItemAdapter(getActivity(), videoDataModelsList);
        rcVideo.setAdapter(videoItemAdapter);
        videoItemAdapter.notifyDataSetChanged();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onResume() {
        super.onResume();
        startTime = System.currentTimeMillis();
    }

    public static String msToString(long ms) {
        long totalSecs = ms/1000;
        long hours = (totalSecs / 3600);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00"
                : ((mins < 10)
                ? "0" + mins
                : "" + mins);
        String secsString = (secs == 0)
                ? "00"
                : ((secs < 10)
                ? "0" + secs
                : "" + secs);
        if (hours > 0)
            return hours+" "+"hr" + ":" + minsString+" "+"min" + ":" + secsString+" "+"sec";
        else if (mins > 0)
            return mins+" "+"min" + ":" + secsString+" "+"sec";
        else return secsString+" "+"sec";
    }
    @Override
    public void onPause() {
        super.onPause();
        TimeSpendModelDb timeSpendModelDbs;
        timeSpendModelDbs = dbHelper.getTImeList(subBrandId);
        totalTime = timeSpendModelDbs.getVideoTime();
        endTime = System.currentTimeMillis();
        long timeSpend = endTime - startTime;
        SharedPreferenceManager.setVideoTime(getActivity(), timeSpend+totalTime);
        dbHelper.updateTime(new TimeSpendModelDb(variantId, "1",timeSpendModelDbs.getPackTime(), timeSpendModelDbs.getPhotoTime(),timeSpend+totalTime,timeSpendModelDbs.getEngTime()));
//        editor = sharedPreferences.edit();
//        editor.putLong("DetailScreen3Time", timeSpend+totalTime).apply(); // Storing long
        Log.d("Fragment 3 Time:", msToString(timeSpend+totalTime));
        //Toast.makeText(getActivity(),  "Video: "+msToString(timeSpend+totalTime), Toast.LENGTH_SHORT).show();
    }

}
