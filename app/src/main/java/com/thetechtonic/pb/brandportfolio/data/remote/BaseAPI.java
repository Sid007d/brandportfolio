package com.thetechtonic.pb.brandportfolio.data.remote;




import com.thetechtonic.pb.brandportfolio.MyApplication;
import com.thetechtonic.pb.brandportfolio.data.event.BaseEvent;
import com.thetechtonic.pb.brandportfolio.data.event.EmptyEvent;
import com.thetechtonic.pb.brandportfolio.data.event.ErrorEvent;
import com.thetechtonic.pb.brandportfolio.utils.constants.I;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Response;

/**
 * Created by bukhoriaqid on 11/12/16.
 */

abstract class BaseAPI
{
    MyApplication app   = MyApplication.getInstance();
    private EventBus      event = EventBus.getDefault();

    private EmptyEvent mEmptyEvent = new EmptyEvent();

    void handleResponse (Response response, BaseEvent eventClass)
    {
        if (response.isSuccessful())
        {
            if (response.code() == I.HTTP_NO_CONTENT)
            {
                event.post(mEmptyEvent);
            }
            else
            {
                event.post(eventClass);
            }
        }
        else
        {
            event.post(new ErrorEvent(response));
        }
    }


}