package com.thetechtonic.pb.brandportfolio.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.data.DataManager;
import com.thetechtonic.pb.brandportfolio.data.SessionManager;
import com.thetechtonic.pb.brandportfolio.data.event.BrandListSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.ErrorEvent;
import com.thetechtonic.pb.brandportfolio.data.event.SubBrandSuccessEvent;
import com.thetechtonic.pb.brandportfolio.models.FormattedTime;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataList;
import com.thetechtonic.pb.brandportfolio.models.OfflineSubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.SubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.utils.CheckForSDCard;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;
import com.thetechtonic.pb.brandportfolio.utils.Utils;
import com.thetechtonic.pb.brandportfolio.utils.constants.S;
import com.thetechtonic.pb.brandportfolio.view.adapter.HorizontalPagerAdapter;
import com.thetechtonic.pb.brandportfolio.view.adapter.MyPagerAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;


public class VariantActivity extends AppCompatActivity {

    long startTime;
    long endTime;
    private List<SubBrandResponse.SubBrandList> subBrandLists = new ArrayList<>();
    private List<OfflineDataList> offlineDataLists;
    private String brandId;
    EventBus event;
    TextView tvTimeSubmit;
    private static ProgressDialog progressDialog;
    ImageView ivBack, ivBrandImage, ivWarning;
    DBHelper dbHelper;
    private ImageView ivBrandLogo;
    private LinearLayout llTab, llOne, llTwo;
    private TextView tvOne, tvTwo;
    private View viewOne, viewTwo;
    private RecyclerView recyclerView;
    private CarouselLayoutManager layoutManager;
    private ProgressDialog mProgressDialog1;
    private static String downloadFileName = "";
    private List<OfflineSubBrandResponse> subBrandData = new ArrayList<>();
    private long getpackTime=0, getPhotoTime=0,getVideoTime=0,getEngTime=0;
    String userId;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_variant);
        dbHelper = new DBHelper(this);
        progressDialog = new ProgressDialog(this);
        brandId = getIntent().getStringExtra("brandId");
        event = EventBus.getDefault();
        ivBrandLogo = findViewById(R.id.ivBrandLogo);
        initUI();
        inEvent();
    }

    public void initUI() {
        ivBack = findViewById(R.id.ivBack);
        ivWarning = findViewById(R.id.ivWarninng);
        ivBrandImage = findViewById(R.id.ivBrandLogo);
        llTab = findViewById(R.id.llTab);
        llOne = findViewById(R.id.ll1);
        llTwo = findViewById(R.id.llTwo);
        tvOne = findViewById(R.id.tvOne);
        tvTwo = findViewById(R.id.tvTwo);
        viewOne = findViewById(R.id.viewOne);
        viewTwo = findViewById(R.id.viewTwo);
         recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ivBack.setOnClickListener(view -> finish());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)

    public void inEvent() {
        // Initialize the progress dialog
        mProgressDialog1 = new ProgressDialog(this);
        mProgressDialog1.setIndeterminate(false);
        mProgressDialog1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog1.setTitle("Downloading SubBrands Photos");
        mProgressDialog1.setMessage("Please wait, we are downloading your files...");
        mProgressDialog1.setCancelable(false);
        sharedPreferences = getSharedPreferences(S.MyPREFERENCES, Context.MODE_PRIVATE);
        userId = sharedPreferences.getString(S.ID, "");

        String language = SharedPreferenceManager.getLanguage(this);
        if (language.equals("en")) {
            ivWarning.setImageResource(R.mipmap.hindi);
        } else if (language.equals("hi")) {
            ivWarning.setImageResource(R.mipmap.hindi);
        } else if (language.equals("bn")) {
            ivWarning.setImageResource(R.mipmap.bengali);
        } else if (language.equals("as")) {
            ivWarning.setImageResource(R.mipmap.assamese);
        } else if (language.equals("brx")) {
            ivWarning.setImageResource(R.mipmap.bodo);
        } else if (language.equals("gu")) {
            ivWarning.setImageResource(R.mipmap.gujrati);
        } else if (language.equals("kn")) {
            ivWarning.setImageResource(R.mipmap.kannada);
        } else if (language.equals("ml")) {
            ivWarning.setImageResource(R.mipmap.malyalam);
        } else if (language.equals("mr")) {
            ivWarning.setImageResource(R.mipmap.marathi);
        } else if (language.equals("or")) {
            ivWarning.setImageResource(R.mipmap.oriya);
        } else if (language.equals("ta")) {
            ivWarning.setImageResource(R.mipmap.tamil);
        } else if (language.equals("te")) {
            ivWarning.setImageResource(R.mipmap.telugu);
        } else if (language.equals("ur")) {
            ivWarning.setImageResource(R.mipmap.urdu);
        }

        if (Objects.requireNonNull(brandId).equals("1")) {
            ivBrandImage.setImageResource(R.mipmap.classic);
        } else if (brandId.equals("2")) {
            ivBrandImage.setImageResource(R.mipmap.gold_flake_trans);
        } else if (brandId.equals("3")) {
            llTab.setVisibility(View.VISIBLE);
            ivBrandImage.setImageResource(R.mipmap.hollywood);
        } else {

            ivBrandImage.setImageResource(R.mipmap.american);
        }
         layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true);


    /*final HorizontalInfiniteCycleViewPager horizontalInfiniteCycleViewPager = findViewById(R.id.hicvp);
    horizontalInfiniteCycleViewPager.setAdapter(new HorizontalPagerAdapter(this, false,subBrandLists));*/
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true);
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        layoutManager.setMaxVisibleItems(2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView.addOnScrollListener(new CenterScrollListener());

        llTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOne.setTextColor(Color.parseColor("#ffffff"));
                viewOne.setBackgroundColor(Color.parseColor("#ffffff"));
                tvTwo.setTextColor(Color.parseColor("#c79c2c"));
                viewTwo.setBackgroundColor(Color.parseColor("#c79c2c"));
            }
        });

        llOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOne.setTextColor(Color.parseColor("#c79c2c"));
                viewOne.setBackgroundColor(Color.parseColor("#c79c2c"));
                tvTwo.setTextColor(Color.parseColor("#ffffff"));
                viewTwo.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        });
        subBrandData = dbHelper.getsubBrandList(brandId);
        if (subBrandData.isEmpty()){
            callSubBrandList();
        }else {
            recyclerView.setAdapter(new MoviesAdapter(subBrandData));
        }

    }

    public void callSubBrandList(){
        showLoading("Please wait...");
        RequestBody brandIdBody = RequestBody.create(MediaType.parse("text/plain"), brandId);
        DataManager.getSubBranList(brandIdBody);
    }

    public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

        private List<OfflineSubBrandResponse> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView;

            public MyViewHolder(View view) {
                super(view);
                imageView = (ImageView) view.findViewById(R.id.img_item);

            }
        }


        public MoviesAdapter(List<OfflineSubBrandResponse> moviesList) {
            this.moviesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            OfflineSubBrandResponse movie = moviesList.get(position);
            File f = new File(movie.getSubBrandPhoto());
            Bitmap b = null;
            try {
                b = BitmapFactory.decodeStream(new FileInputStream(f));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            holder.imageView.setImageBitmap(b);
            /*Glide.with(VariantActivity.this).load(movie.getSubBrandPhoto())
                    .into(holder.imageView);*/

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(VariantActivity.this, DetailActivity.class);
                    intent.putExtra("subBrandId", moviesList.get(position).getSubBrandId());
                    intent.putExtra("variantId", moviesList.get(position).getVariant_id());
                    intent.putExtra("subBrandName", moviesList.get(position).getSubBrandName());
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }
    }

    @Subscribe
    public void onSuccess(SubBrandSuccessEvent event) {
        hideLoading();
        subBrandLists = event.getBrandList();



        for (int i=0;i<subBrandLists.size();i++){
            String path1 = subBrandLists.get(i).getSubBrandPhoto().replace(Utils.subBrandsUrl, "");
            String basePath1 = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory +"/" + "SubBrands";

            dbHelper.insertSubBrand(new OfflineSubBrandResponse(brandId,subBrandLists.get(i).getSubBrandId(), subBrandLists.get(i).getSubBrandName(),subBrandLists.get(i).getVariant_id(), basePath1+"/"+path1));
            dbHelper.insertTime(new TimeSpendModelDb(subBrandLists.get(i).getVariant_id(), userId,getpackTime, getPhotoTime,getVideoTime,getEngTime));
            dbHelper.insertFormattedTime(new FormattedTime(subBrandLists.get(i).getVariant_id(), userId ,subBrandLists.get(i).getSubBrandName(),"0", "0","0","0"));
            new DownloadTask().execute(subBrandLists.get(i).getSubBrandPhoto());
        }
    }


    @Subscribe
    public void onFailed(ErrorEvent event) {
        hideLoading();
        Toast.makeText(this, event.getMessage(), Toast.LENGTH_SHORT).show();

    }

    public static void showLoading (String message)
    {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void hideLoading ()
    {
        if (progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        event.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        event.unregister(this);
    }

    private class DownloadTask extends AsyncTask<String, String, String> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog1.setMax(subBrandLists.size());
            mProgressDialog1.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            for (int i=0;i<subBrandLists.size();i++) {
                try {
                    downloadFileName = arg0[0].replace(Utils.subBrandsUrl, "");
                    URL url = new URL(arg0[0]);//Create Download URl
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                    c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                    c.connect();//connect the URL Connection
                    int lenghtOfFile = c.getContentLength();
                    //If Connection response is not OK then show Logs
                    if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.e("TAG", "Server returned HTTP " + c.getResponseCode()
                                + " " + c.getResponseMessage());

                    }


                    //Get File if SD card is present
                    if (new CheckForSDCard().isSDCardPresent()) {

                        apkStorage = new File(Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + "SubBrands");
                    } else
                        Toast.makeText(VariantActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                    //If File is not present create directory
                    if (!apkStorage.exists()) {
                        apkStorage.mkdir();
                        Log.e("TAG", "Directory Created.");
                    }

                    outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                    //Create New File if not present
                    if (!outputFile.exists()) {
                        outputFile.createNewFile();
                        Log.e("TAG", "File Created");
                    }

                    FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                    InputStream is = c.getInputStream();//Get InputStream for connection

                    byte[] buffer = new byte[1024];//Set buffer type
                    int len1 = 0;//init length
                    long total = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        total += len1;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                        fos.write(buffer, 0, len1);//Write new file
                    }

                    //Close all connection after doing task
                    fos.close();
                    is.close();

                } catch (Exception e) {

                    //Read exception if something went wrong
                    e.printStackTrace();
                    outputFile = null;
                    Log.e("TAG", "Download Error Exception " + e.getMessage());
                }
            }
            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {


        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            subBrandLists.remove(0);
            if (subBrandLists.isEmpty()){
                mProgressDialog1.dismiss();
                List<OfflineSubBrandResponse> subBrandLists1 = dbHelper.getsubBrandList(brandId);
                recyclerView.setAdapter(new MoviesAdapter(subBrandLists1));
            }else {
                mProgressDialog1.incrementProgressBy(1);
            }
            //Toast.makeText(MainActivity.this,"Complete",Toast.LENGTH_SHORT).show();
        }
    }

}
