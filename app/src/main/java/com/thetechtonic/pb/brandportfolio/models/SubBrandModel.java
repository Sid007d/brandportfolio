package com.thetechtonic.pb.brandportfolio.models;

public class SubBrandModel {

    String subBrandId;
    String subBrandName;
    String subBrandPhoto;



    public SubBrandModel(String subBrandId,String subBrandName,String subBrandPhoto)
    {
        this.subBrandId = subBrandId;
        this.subBrandName = subBrandName;
        this.subBrandPhoto = subBrandPhoto;
    }
   public SubBrandModel()
   {

   }
    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getSubBrandName() {
        return subBrandName;
    }

    public void setSubBrandName(String subBrandName) {
        this.subBrandName = subBrandName;
    }

    public String getSubBrandPhoto() {
        return subBrandPhoto;
    }

    public void setSubBrandPhoto(String subBrandPhoto) {
        this.subBrandPhoto = subBrandPhoto;
    }
}
