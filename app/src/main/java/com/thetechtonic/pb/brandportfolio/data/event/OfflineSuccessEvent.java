package com.thetechtonic.pb.brandportfolio.data.event;


import com.thetechtonic.pb.brandportfolio.models.OfflineDataList;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataResponse;


import java.util.ArrayList;
import java.util.List;

public class OfflineSuccessEvent extends BaseEvent {

    private String message,code;
    private String flagstatus,flagTime,deciceId;
    private List<OfflineDataList> offlineDataLists=new ArrayList<>();

    public OfflineSuccessEvent(OfflineDataResponse offlineDataResponse) {

        message=offlineDataResponse.getResponseMessage();
        code=offlineDataResponse.getResponseCode();
        flagstatus=offlineDataResponse.getFlagStatus();
        flagTime=offlineDataResponse.getFlagTime();
        deciceId=offlineDataResponse.getDeviceId();
        offlineDataLists=offlineDataResponse.getData();
        }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFlagstatus() {
        return flagstatus;
    }

    public void setFlagstatus(String flagstatus) {
        this.flagstatus = flagstatus;
    }

    public String getFlagTime() {
        return flagTime;
    }

    public void setFlagTime(String flagTime) {
        this.flagTime = flagTime;
    }

    public String getDeciceId() {
        return deciceId;
    }

    public void setDeciceId(String deciceId) {
        this.deciceId = deciceId;
    }

    public List<OfflineDataList> getOfflineDataLists() {
        return offlineDataLists;
    }

    public void setOfflineDataLists(List<OfflineDataList> offlineDataLists) {
        this.offlineDataLists = offlineDataLists;
    }
}
