package com.thetechtonic.pb.brandportfolio.models;

public class Cigarette {

    public int imageResId;
    public int titleResId;

    public Cigarette (int imageResId, int titleResId){
        this.imageResId = imageResId;
        this.titleResId = titleResId;
    }
}
