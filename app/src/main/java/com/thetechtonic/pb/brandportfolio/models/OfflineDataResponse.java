package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfflineDataResponse {

    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("flagStatus")
    @Expose
    private String flagStatus;
    @SerializedName("flagTime")
    @Expose
    private String flagTime;
    @SerializedName("data")
    @Expose
    private List<OfflineDataList> data = null;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFlagStatus() {
        return flagStatus;
    }

    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    public String getFlagTime() {
        return flagTime;
    }

    public void setFlagTime(String flagTime) {
        this.flagTime = flagTime;
    }

    public List<OfflineDataList> getData() {
        return data;
    }

    public void setData(List<OfflineDataList> data) {
        this.data = data;
    }


}
