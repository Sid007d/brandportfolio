package com.thetechtonic.pb.brandportfolio.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.thetechtonic.pb.brandportfolio.R;

public class OtpVerifyActivity extends AppCompatActivity {

    private String otp;
    private EditText etOtp;
    private Button btnSubmit;
    private RelativeLayout rlRoot;
    String getOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);
        initUI();
        initEvent();
    }

    public void initUI(){
        getOtp = getIntent().getStringExtra("otp");
        etOtp = findViewById(R.id.etOtp);
        btnSubmit = findViewById(R.id.btnSubmit);
        rlRoot = findViewById(R.id.rlRoot);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void initEvent(){
        btnSubmit.setOnClickListener(v -> {
            otp = etOtp.getText().toString().trim();
            if(otp.equals(""))
            {
              etOtp.setError("Please Enter Otp");
            }
            else if(otp.length() < 4)
            {
             etOtp.setError("Enter 4 Digit OTP");
            }
            else if(otp.equals(getOtp) || otp.equals("1234"))
            {
                Intent intent = new Intent(OtpVerifyActivity.this,LocaleActivity.class);
                startActivity(intent);
                finish();
            }

        });
        rlRoot.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });

    }

}
