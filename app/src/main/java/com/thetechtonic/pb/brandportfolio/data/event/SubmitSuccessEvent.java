package com.thetechtonic.pb.brandportfolio.data.event;


import com.thetechtonic.pb.brandportfolio.models.SubmitDataResponse;

public class SubmitSuccessEvent extends BaseEvent {

    String message;



    public SubmitSuccessEvent(SubmitDataResponse submitDataResponse) {

        message=submitDataResponse.getMessage();

    }


    public String getMessage() {
        return message;
    }

}

