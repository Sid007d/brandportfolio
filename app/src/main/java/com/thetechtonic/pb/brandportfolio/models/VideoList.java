package com.thetechtonic.pb.brandportfolio.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoList implements Parcelable{

    @SerializedName("subBrandDetailId")
    @Expose
    private String subBrandDetailId;
    @SerializedName("subBrandId")
    @Expose
    private String subBrandId;
    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("fileName")
    @Expose
    private String fileName;


    public VideoList(String subBrandId, String thumbnail, String fileName) {
        this.subBrandId = subBrandId;
        this.thumbnail = thumbnail;
        this.fileName = fileName;
    }



    public VideoList(Parcel in) {
        subBrandDetailId = in.readString();
        subBrandId = in.readString();
        brandId = in.readString();
        category = in.readString();
        caption = in.readString();
        thumbnail = in.readString();
        fileName = in.readString();
    }

    public static final Creator<VideoList> CREATOR = new Creator<VideoList>() {
        @Override
        public VideoList createFromParcel(Parcel in) {
            return new VideoList(in);
        }

        @Override
        public VideoList[] newArray(int size) {
            return new VideoList[size];
        }
    };

    public VideoList() {

    }

    public String getSubBrandDetailId() {
        return subBrandDetailId;
    }

    public void setSubBrandDetailId(String subBrandDetailId) {
        this.subBrandDetailId = subBrandDetailId;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(subBrandDetailId);
        parcel.writeString(subBrandId);
        parcel.writeString(brandId);
        parcel.writeString(category);
        parcel.writeString(caption);
        parcel.writeString(thumbnail);
        parcel.writeString(fileName);
    }
}
