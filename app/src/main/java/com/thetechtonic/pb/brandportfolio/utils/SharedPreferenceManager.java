package com.thetechtonic.pb.brandportfolio.utils;

import android.content.Context;
import android.preference.PreferenceManager;

public class SharedPreferenceManager {

    public static void setPackTime(Context context, long pack_Time) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong("packtime", pack_Time).commit();
    }

    public static long getPackTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("packtime", 0);

    }
    public static void setPhotoTime(Context context, long photo_Time) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong("phototime", photo_Time).commit();
    }

    public static long getPhotoTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("phototime", 0);

    }

    public static void setVideoTime(Context context, long video_Time) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong("videotime", video_Time).commit();
    }

    public static long getVideoTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("videotime", 0);

    }
    public static void setEngTime(Context context, long eng_Time) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong("engtime", eng_Time).commit();
    }

    public static long getEngTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("engtime",0);

    }

    public static void setLanguage(Context context, String language) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("language", language).apply();
    }

    public static String getLanguage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("language","en");

    }
}
