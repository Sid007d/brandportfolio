package com.thetechtonic.pb.brandportfolio.data.event;

import com.thetechtonic.pb.brandportfolio.models.TimeSpendResponse;

/**
 * Created by admin on 9/4/2018.
 */

public class TimeSpendSuccessEvent extends BaseEvent {

    String message;
    String responseCode;


    public TimeSpendSuccessEvent(TimeSpendResponse timeSpendResponse) {
        message=timeSpendResponse.getResponseMessage();
        responseCode=timeSpendResponse.getResponseCode();
    }


    public String getMessage() {
        return message;
    }

    public String getResponseCode() {
        return responseCode;
    }

}
