package com.thetechtonic.pb.brandportfolio.models;

public class TimeSpendModelDb {


    String subBrandId;
    long packTime;
    long photoTime;
    long videoTime;
    long engTime;
    String userId;

    public TimeSpendModelDb(String subBrandId,String userId, long packTime, long photoTime, long videoTime, long engTime) {
        this.subBrandId = subBrandId;
        this.userId=userId;
        this.packTime = packTime;
        this.photoTime = photoTime;
        this.videoTime = videoTime;
        this.engTime = engTime;
    }

    public TimeSpendModelDb() {
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public long getPackTime() {
        return packTime;
    }

    public void setPackTime(long packTime) {
        this.packTime = packTime;
    }

    public long getPhotoTime() {
        return photoTime;
    }

    public void setPhotoTime(long photoTime) {
        this.photoTime = photoTime;
    }

    public long getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(long videoTime) {
        this.videoTime = videoTime;
    }

    public long getEngTime() {
        return engTime;
    }

    public void setEngTime(long engTime) {
        this.engTime = engTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
