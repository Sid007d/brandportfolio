package com.thetechtonic.pb.brandportfolio.models;

/**
 * Created by admin on 8/29/2018.
 */

public class PhotoDataModel {

    String title;
    int fileName;

    public PhotoDataModel(String title, int fileName) {
        this.title = title;
        this.fileName=fileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFileName() {
        return fileName;
    }

    public void setFileName(int fileName) {
        this.fileName = fileName;
    }
}
