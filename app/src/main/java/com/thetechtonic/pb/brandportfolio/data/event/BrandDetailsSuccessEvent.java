package com.thetechtonic.pb.brandportfolio.data.event;

import com.thetechtonic.pb.brandportfolio.models.BrandDetailsResponse;
import com.thetechtonic.pb.brandportfolio.models.EngagementList;
import com.thetechtonic.pb.brandportfolio.models.PackList;
import com.thetechtonic.pb.brandportfolio.models.PhotoList;
import com.thetechtonic.pb.brandportfolio.models.VideoList;

import java.util.ArrayList;
import java.util.List;

public class BrandDetailsSuccessEvent extends BaseEvent {

    private String message;
    private String responseCode;
    private String videoUrl;
    private PackList packList;
    private List<PhotoList> photoLists=new ArrayList<>();
    private List<VideoList> videoLists=new ArrayList<>();
    private List<EngagementList> engagementLists=new ArrayList<>();

    public BrandDetailsSuccessEvent(BrandDetailsResponse brandDetailsResponse) {

        message=brandDetailsResponse.getResponseMessage();
        responseCode=brandDetailsResponse.getResponseCode();
   //     videoUrl=brandDetailsResponse.getPack().getFileName();
        photoLists=brandDetailsResponse.getPhoto();
        videoLists=brandDetailsResponse.getVideo();
        engagementLists=brandDetailsResponse.getEngagement();
        packList = brandDetailsResponse.getPack();

    }

    public PackList getPackList() {
        return packList;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getMessage() {
        return message;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public List<PhotoList> getPhotoLists() {
        return photoLists;
    }

    public List<VideoList> getVideoLists() {
        return videoLists;
    }

    public List<EngagementList> getEngagementLists() {
        return engagementLists;
    }
}
