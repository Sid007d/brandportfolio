package com.thetechtonic.pb.brandportfolio.view.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.models.PhotoList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 8/29/2018.
 */

public class PhotoItemAdapter  extends RecyclerView.Adapter<PhotoItemAdapter.ViewHolder> {

    private List<PhotoList> profileAdapterList = new ArrayList<PhotoList>();
    Context context;

    public PhotoItemAdapter(Context context,List<PhotoList> profileAdapterList) {
        this.profileAdapterList = profileAdapterList;
        this.context = context;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image1 = itemView.findViewById(R.id.image1);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.photo_view, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        PhotoList uploadPhotoList = profileAdapterList.get(i);
//        viewHolder.image1.setImageResource(all.imageId);

        Glide.with(context).load(uploadPhotoList.getFileName()).into(viewHolder.image1);

        viewHolder.image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.preview_image);
                ImageView btnClose = (ImageView) nagDialog.findViewById(R.id.btnIvClose);
                ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);

                Glide.with(context).load(uploadPhotoList.getFileName()).into(ivPreview);

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return profileAdapterList.size();
    }
}

