package com.thetechtonic.pb.brandportfolio.view.activity;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.models.OfflinePack;
import com.thetechtonic.pb.brandportfolio.utils.CheckForSDCard;
import com.thetechtonic.pb.brandportfolio.utils.Utils;
import com.thetechtonic.pb.brandportfolio.view.fragment.PackFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class VideoPlayActivity extends AppCompatActivity {


    private String videoUrl;
    private ImageView ivBack;
    VideoView videoplayer;
    private MediaPlayer mediaPlayer;
    private List<String> fileList;
    List<OfflinePack> offlinePackList;
    private ProgressDialog mProgressDialog3;
    DBHelper dbHelper;
    private String subBrandId;
    private String downloadFileName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        ivBack = findViewById(R.id.ivBack);
        dbHelper = new DBHelper(this);
        mProgressDialog3 = new ProgressDialog(this);
        mProgressDialog3.setIndeterminate(false);
        mProgressDialog3.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog3.setTitle("Downloading Pack Sub Video");
        mProgressDialog3.setMessage("Please wait, we are downloading your files...");
        mProgressDialog3.setCancelable(false);
        subBrandId = getIntent().getStringExtra("subBrandId");
        videoUrl = getIntent().getStringExtra("videoUrl");
        String path = videoUrl.replace(Utils.subVideoUrl, "");
        String basePath = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + Utils.downloadSubVideos;
        videoplayer = findViewById(R.id.videoplayer);

        offlinePackList = dbHelper.getPackSubVideoList(subBrandId);
        if (offlinePackList.isEmpty()) {
            dbHelper.insertPackSubVideo(new OfflinePack(subBrandId, basePath + "/" + path));
            new PackSubVideoDownloadingTask().execute(videoUrl);
        } else {
            fileList = new ArrayList<>();
            for (int i = 0; i < offlinePackList.size(); i++) {
                String searchUrl = offlinePackList.get(i).getFileName().replace(basePath, "");
                fileList.add(searchUrl);
            }

            if (fileList.contains("/"+path)){
                videoplayer.setVideoPath(basePath + "/" + path);
                videoplayer.requestFocus();
                videoplayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                videoplayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        Log.d("API123", "What " + what + " extra " + extra);
                        return false;
                    }
                });
                videoplayer.start();
            }

        }
    }

    private class PackSubVideoDownloadingTask extends AsyncTask<String, String, String> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog3.setMax(1);
            mProgressDialog3.show();
        }

        @Override
        protected String doInBackground(String... arg0) {

            try {
                downloadFileName = arg0[0].replace(Utils.subVideoUrl, "");
                URL url = new URL(arg0[0]);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                int lenghtOfFile = c.getContentLength();
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("TAG", "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + Utils.downloadSubVideos);
                } else
                    Toast.makeText(VideoPlayActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e("TAG", "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("TAG", "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                long total = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e("TAG", "Download Error Exception " + e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {


        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (outputFile!=null) {
                mProgressDialog3.dismiss();
                offlinePackList = dbHelper.getPackSubVideoList(subBrandId);
                String path = videoUrl.replace(Utils.subVideoUrl, "");
                String basePath = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory + "/" + Utils.downloadSubVideos;

                fileList = new ArrayList<>();
                for (int i=0;i<offlinePackList.size();i++){
                    String searchUrl = offlinePackList.get(i).getFileName().replace(basePath,"");
                    fileList.add(searchUrl);
                }

                if (fileList.contains("/"+path)){
                    videoplayer.setVideoPath(basePath + "/" + path);
                    videoplayer.requestFocus();
                    videoplayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.setLooping(true);
                        }
                    });
                    videoplayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.d("API123", "What " + what + " extra " + extra);
                            return false;
                        }
                    });
                    videoplayer.start();
                }

            }
        }
    }

}
