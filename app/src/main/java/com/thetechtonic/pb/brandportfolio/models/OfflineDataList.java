package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfflineDataList {

    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("brandName")
    @Expose
    private String brandName;
    @SerializedName("brandVideo")
    @Expose
    private String brandVideo;
    @SerializedName("subBrands")
    @Expose
    private List<OfflineSubBrandResponse> subBrands = null;

    public OfflineDataList(String brandId, String brandName, String brandVideo) {
        this.brandId = brandId;
        this.brandName = brandName;
        this.brandVideo = brandVideo;
    }

    public OfflineDataList() {
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandVideo() {
        return brandVideo;
    }

    public void setBrandVideo(String brandVideo) {
        this.brandVideo = brandVideo;
    }

    public List<OfflineSubBrandResponse> getSubBrands() {
        return subBrands;
    }

    public void setSubBrands(List<OfflineSubBrandResponse> subBrands) {
        this.subBrands = subBrands;
    }


}
