package com.thetechtonic.pb.brandportfolio.data.remote;


import com.google.gson.JsonObject;
import com.thetechtonic.pb.brandportfolio.data.event.BrandDetailsSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.BrandListSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.ErrorEvent;
import com.thetechtonic.pb.brandportfolio.data.event.OfflineSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.OfflineV2SuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.SubBrandSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.SubmitSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.TimeSpendSuccessEvent;
import com.thetechtonic.pb.brandportfolio.models.BrandDetailsResponse;
import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataResponse;
import com.thetechtonic.pb.brandportfolio.models.RawUserData;
import com.thetechtonic.pb.brandportfolio.models.SubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.SubmitDataResponse;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bukhoriaqid on 11/11/16.
 */

public class AuthAPI extends BaseAPI
{

    private EventBus event = EventBus.getDefault();


    //USER LOGIN getSubBranList
    public void getBranList() {

        app.tAPIService.getBranList().enqueue(new Callback<BrandListResponse>() {
            @Override
            public void onResponse(Call<BrandListResponse> call, Response<BrandListResponse> response) {
                BrandListResponse brandListResponse = (BrandListResponse) response.body();

                if (response.body().getResponseCode().equals("200")) {
                    handleResponse(response, new BrandListSuccessEvent(brandListResponse));
                } else {
                    handleResponse(response, new ErrorEvent(brandListResponse));
                }

            }

            @Override
            public void onFailure(Call<BrandListResponse> call, Throwable t) {
                event.post(new ErrorEvent(t));
            }
        });
    }
        //USER LOGIN  getSubBradDetails
        public void getSubBranList(RequestBody brandId) {

            app.tAPIService.getSubBranList(brandId).enqueue(new Callback<SubBrandResponse>() {
                @Override
                public void onResponse(Call<SubBrandResponse> call, Response<SubBrandResponse> response) {
                    SubBrandResponse subBrandResponse = (SubBrandResponse) response.body();

                    if (response.body().getResponseCode().equals("200")) {
                        handleResponse(response, new SubBrandSuccessEvent(subBrandResponse));
                    } else {
                        handleResponse(response, new ErrorEvent(subBrandResponse));
                    }

                }

                @Override
                public void onFailure(Call<SubBrandResponse> call, Throwable t) {
                    event.post(new ErrorEvent(t));
                }
            });

        }

    //getSubBradDetails  callTime
    public void getSubBradDetails(RequestBody brandId) {

        app.tAPIService.getSubBradDetails(brandId).enqueue(new Callback<BrandDetailsResponse>() {
            @Override
            public void onResponse(Call<BrandDetailsResponse> call, Response<BrandDetailsResponse> response) {
                BrandDetailsResponse brandDetailsResponse = (BrandDetailsResponse) response.body();

                if (response.body().getResponseCode().equals("200")) {
                    handleResponse(response, new BrandDetailsSuccessEvent(brandDetailsResponse));
                } else {
                    handleResponse(response, new ErrorEvent(brandDetailsResponse));
                }

            }

            @Override
            public void onFailure(Call<BrandDetailsResponse> call, Throwable t) {
                event.post(new ErrorEvent(t));
            }
        });

    }
    //callTime
    public void callTime(RequestBody customerId, RequestBody customerName,RequestBody brandId,RequestBody subBrandPackTime,RequestBody subBrandPhotoTime,RequestBody subBrandVideoTime,RequestBody subBrandEngagementTime) {

        app.tAPIService.callTime(customerId,customerName,brandId,subBrandPackTime,subBrandPhotoTime,subBrandVideoTime,subBrandEngagementTime).enqueue(new Callback<TimeSpendResponse>() {
            @Override
            public void onResponse(Call<TimeSpendResponse> call, Response<TimeSpendResponse> response) {
                TimeSpendResponse timeSpendResponse = (TimeSpendResponse) response.body();
                 if (timeSpendResponse == null){
                     handleResponse(response, new ErrorEvent("In Process"));
                 } else {
                     if (response.body().getResponseCode().equals("200")) {
                         handleResponse(response, new TimeSpendSuccessEvent(timeSpendResponse));
                     } else {
                         handleResponse(response, new ErrorEvent(timeSpendResponse));
                     }
                 }


            }

            @Override
            public void onFailure(Call<TimeSpendResponse> call, Throwable t) {
                event.post(new ErrorEvent(t));
            }
        });

    }
//getofflinedata
    public void getofflinedata(RequestBody deviceId) {

        app.tAPIService.getofflinedata(deviceId).enqueue(new Callback<OfflineDataResponse>() {
            @Override
            public void onResponse(Call<OfflineDataResponse> call, Response<OfflineDataResponse> response) {
                OfflineDataResponse offlineDataResponse = (OfflineDataResponse) response.body();

                if (response.body().getResponseCode().equals("200")) {
                    handleResponse(response, new OfflineSuccessEvent(offlineDataResponse));
                } else {
                    handleResponse(response, new ErrorEvent(offlineDataResponse));
                }
            }
            @Override
            public void onFailure(Call<OfflineDataResponse> call, Throwable t) {
                event.post(new ErrorEvent(t));
            }
        });
    }

    //getofflinedata
    public void getofflinedataV2(RequestBody deviceId) {

        app.tAPIService.getofflinedataV2(deviceId).enqueue(new Callback<OfflineDataResponse>() {
            @Override
            public void onResponse(Call<OfflineDataResponse> call, Response<OfflineDataResponse> response) {
                OfflineDataResponse offlineDataResponse = (OfflineDataResponse) response.body();

                if (response.body().getResponseCode().equals("200")) {
                    handleResponse(response, new OfflineV2SuccessEvent(offlineDataResponse));
                } else {
                    handleResponse(response, new ErrorEvent(offlineDataResponse));
                }
            }
            @Override
            public void onFailure(Call<OfflineDataResponse> call, Throwable t) {
                event.post(new ErrorEvent(t));
            }
        });
    }

    public void submitRawData(RawUserData rawUserData) {

        app.itcAPIService.submitRawData(rawUserData).enqueue(new Callback<SubmitDataResponse>() {
            @Override
            public void onResponse(Call<SubmitDataResponse> call, Response<SubmitDataResponse> response) {
                SubmitDataResponse submitDataResponse = (SubmitDataResponse) response.body();

                if (Objects.requireNonNull(response.body()).getSuccess()) {
                    handleResponse(response, new SubmitSuccessEvent(submitDataResponse));
                } else {
                    handleResponse(response, new ErrorEvent(submitDataResponse));
                }
            }
            @Override
            public void onFailure(Call<SubmitDataResponse> call, Throwable t) {
                event.post(new ErrorEvent(t));
            }
        });
    }
}
