package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfflineVideoList {

    @SerializedName("subBrandDetailId")
    @Expose
    private String subBrandDetailId;
    @SerializedName("subBrandId")
    @Expose
    private String subBrandId;
    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("fileName")
    @Expose
    private String fileName;

    public OfflineVideoList(String brandId, String subBrandId, String subBrandDetailId,   String category, String fileName) {
        this.subBrandDetailId = subBrandDetailId;
        this.subBrandId = subBrandId;
        this.brandId = brandId;
        this.category = category;
        this.fileName = fileName;
    }
    public OfflineVideoList()
    {

    }
    public String getSubBrandDetailId() {
        return subBrandDetailId;
    }

    public void setSubBrandDetailId(String subBrandDetailId) {
        this.subBrandDetailId = subBrandDetailId;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


}
