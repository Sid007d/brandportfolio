package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackList {

    @SerializedName("subBrandDetailId")
    @Expose
    private String subBrandDetailId;
    @SerializedName("subBrandId")
    @Expose
    private String subBrandId;
    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("packInfo")
    @Expose
    private String packInfo;

    public String getPackInfo() {
        return packInfo;
    }

    public void setPackInfo(String packInfo) {
        this.packInfo = packInfo;
    }

    public String getSubBrandDetailId() {
        return subBrandDetailId;
    }

    public void setSubBrandDetailId(String subBrandDetailId) {
        this.subBrandDetailId = subBrandDetailId;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
