package com.thetechtonic.pb.brandportfolio.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.models.EngagementList;
import com.thetechtonic.pb.brandportfolio.models.PhotoList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 9/4/2018.
 */

public class EngagmentAdapter extends RecyclerView.Adapter<EngagmentAdapter.ViewHolder> {

    private List<EngagementList> engagementLists = new ArrayList<EngagementList>();
    Context context;

    public EngagmentAdapter(Context context,List<EngagementList> engagementLists) {
        this.engagementLists = engagementLists;
        this.context = context;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivEngagment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivEngagment = itemView.findViewById(R.id.ivEngagment);
        }
    }

    @NonNull
    @Override
    public EngagmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.engagmentchild, viewGroup, false);

        return new EngagmentAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EngagmentAdapter.ViewHolder viewHolder, int i) {
        EngagementList engagementList = engagementLists.get(i);
//        viewHolder.image1.setImageResource(all.imageId);

        Glide.with(context).load(engagementList.getThumbnail()).into(viewHolder.ivEngagment);

        viewHolder.ivEngagment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(engagementList.getFileName()); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return engagementLists.size();
    }
}
