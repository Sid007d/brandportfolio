package com.thetechtonic.pb.brandportfolio.data.event;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.thetechtonic.pb.brandportfolio.models.BrandDetailsResponse;
import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataResponse;
import com.thetechtonic.pb.brandportfolio.models.SubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.SubmitDataResponse;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendResponse;
import com.thetechtonic.pb.brandportfolio.utils.constants.I;
import com.thetechtonic.pb.brandportfolio.utils.constants.S;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Sid on 03-06-2018.
 */

public class ErrorEvent extends BaseEvent
{
    String message;

    public ErrorEvent (Response response)
    {
        if (response.code() == I.HTTP_NO_CONTENT)
        {
            message = S.error_tidak_ada_data;
        }
        else if (response.body() instanceof JsonObject) // just in case ?
        {
            JsonObject tempJsonObject = (JsonObject) response.body();
            message = getErrorMessage(tempJsonObject);
        }
        else
        {
            try
            {
                message = deserializeError(response);
            }
            catch (IOException e)
            {
                message = S.error_unknown;
                e.printStackTrace();
            }
        }
    }
public ErrorEvent(BrandListResponse brandListResponse)
{
    message=brandListResponse.getResponseMessage();
}

    public ErrorEvent(SubBrandResponse subBrandResponse)
    {
        message=subBrandResponse.getResponseMessage();
    }
    public ErrorEvent(BrandDetailsResponse brandDetailsResponse)
    {
        message=brandDetailsResponse.getResponseMessage();
    }
    public ErrorEvent(TimeSpendResponse timeSpendResponse)
    {
        message=timeSpendResponse.getResponseMessage();
    }
    public ErrorEvent(String errorMessage)
    {
        message=errorMessage;
    }
    public ErrorEvent(OfflineDataResponse offlineDataResponse)
    {
        message=offlineDataResponse.getResponseMessage();
    }
    public ErrorEvent(SubmitDataResponse offlineDataResponse)
    {
        message=offlineDataResponse.getMessage();
    }

    public ErrorEvent (Throwable t)
    {
        message = S.error_unknown;
        if (t.getMessage() != null)
        {
            if (t.getMessage().contains("connect"))
            {
                message = S.error_connect;
            }
            else if (t.getMessage().contains("204"))
            {
                message = S.error_tidak_ada_data;
            }
            else
            {
                message = t.getMessage();
            }
        }
    }



    public String deserializeError (Response response) throws IOException
    {
        JsonObject errorResponse;

        ResponseBody body    = response.errorBody();
        Gson gson    = new Gson();
        TypeAdapter<JsonObject> adapter = gson.getAdapter(JsonObject.class);
        errorResponse = adapter.fromJson(body.string());

        return getErrorMessage(errorResponse);
    }

    public String getErrorMessage (JsonObject jsonObject)
    {
        String lMessage;
        try
        { // deserialize error as JsonArray
            lMessage = jsonObject.get("message").getAsString();
            JsonArray errors = jsonObject.get("data").getAsJsonObject().get("errors").getAsJsonArray();
            if (errors != null)
            {
                lMessage += " : ";
                for (int i = 0; i < errors.size(); i++)
                {
                    lMessage += errors.get(i).getAsString();
                }
            }
        }
        catch (Exception e)
        {
            lMessage = S.error_unknown;
            e.printStackTrace();
        }
        return lMessage;
    }

    public String getMessage ()
    {
        return message;
    }
}

