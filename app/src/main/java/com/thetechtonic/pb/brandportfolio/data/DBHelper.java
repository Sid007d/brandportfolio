package com.thetechtonic.pb.brandportfolio.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.DataModels;
import com.thetechtonic.pb.brandportfolio.models.EngagementList;
import com.thetechtonic.pb.brandportfolio.models.FormattedTime;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataList;
import com.thetechtonic.pb.brandportfolio.models.OfflineEngList;
import com.thetechtonic.pb.brandportfolio.models.OfflinePack;
import com.thetechtonic.pb.brandportfolio.models.OfflinePhotoList;
import com.thetechtonic.pb.brandportfolio.models.OfflineSubBrandResponse;
import com.thetechtonic.pb.brandportfolio.models.OfflineVideoList;
import com.thetechtonic.pb.brandportfolio.models.PhotoList;
import com.thetechtonic.pb.brandportfolio.models.SubBrandModel;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.models.USERIDMODELS;
import com.thetechtonic.pb.brandportfolio.models.VideoList;

import java.util.ArrayList;

/**
 * Created by admin on 9/6/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    private Context context;
    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "brandportfolio";

    // Main Brand
    private static final String TABLE_BRAND  = "brand";
    private static final String ID="id";
    private static final String BRAND_ID="brandId";
    private static final String COLOUMN_MAINBRAND_NAME="mainBrand";
    private static final String COLOUMN_MAINBRAND_VIDEO="videoUrl";

    // sub Brand
    private static final String TABLE_SUBBRAND="sunbrand";
    private static final String COLOUMN_SUBBRAND_ID="subbrandId";
    private static final String COLOUMN_SUBBRAND_NAME="subbrandName";
    private static final String COLOUMN_SUBBRAND_PHOTO="subbrandPhoto";
    private static final String COLOUMN_SUBBRAND_PACK_VIDEO="subbrandPackVideo";



    // sub branddetails PAck
    private static final String TABLE_SUBBRAND_PACK="sunbrandpack";
    private static final String COLOUMN_SUBBRAND_DETAIL_PACKURL="detailPackurl";

    private static final String TABLE_SUBVIDEO_PACK="subvideopack";
    private static final String COLOUMN_SUBBRAND_SUBVIDEO_PACKURL="subVideoPackurl";

    // sub branddetails Engm
    private static final String TABLE_PHTOT_PACK="photopack";
    private static final String COLOUMN_PHTOT_PACK="coloumphototpack";
    private static final String COLOUMN_PHTOT_THUBNAIL="photothumbnail";


    private static final String TABLE_VIDEO_PACK="videopack";
    private static final String COLOUMN_VIDEO_PACK="coloumVideopack";
    private static final String COLOUMN_VIDEO_THUBNAIL="videohumbnail";


    private static final String TABLE_ENGM_PACK="engmpack";
    private static final String COLOUMN_ENGM_PACK="coloumEngmpack";
    private static final String COLOUMN_ENGM_THUBNAIL="engmhumbnail";

    // sub time table
    private static final String TABLE_TIME="timetable";
    // sub time table
    private static final String TABLE_TIME_FORMAT="formattedtimetable";
    private static final String PACK_TIME="packtime";
    private static final String PHOTO_TIME="phototime";
    private static final String VIDEO_TIME="videotime";
    private static final String ENG_TIME="engtime";
    private static final String USER_ID="userId";

    // USERID LIST
    private static final String TABLE_USER_ID="useridtable";

    // mainBrandTABLE

    private static final String CREATE_TABLE_BRAND="CREATE TABLE IF NOT EXISTS "
            + TABLE_BRAND + "(" + BRAND_ID + " TEXT," + COLOUMN_MAINBRAND_NAME + " TEXT, " +  COLOUMN_MAINBRAND_VIDEO + " TEXT " + ")";

  //   subbrand TAble
    private static final String CREATE_TABLE_SUBBRAND="CREATE TABLE IF NOT EXISTS "
            + TABLE_SUBBRAND + "(" + BRAND_ID + " TEXT," + COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_NAME + " TEXT," + COLOUMN_SUBBRAND_PHOTO + " TEXT," + COLOUMN_SUBBRAND_PACK_VIDEO + " TEXT " + ")";


  //  detailsphoto
//  private static final String CREATE_TABLE_PHOTO_DEAILS="CREATE TABLE IF NOT EXISTS "
//          + TABLE_SUBBRAND_PHOTO + "(" + BRAND_ID + " TEXT," +  COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_PHOTO_ID + " TEXT," + COLOUMN_SUBBRAND_PHOTO_CATEGORY + " TEXT," + COLOUMN_SUBBRAND_DETAIL_PHOTOURL + " TEXT " + ")";

    //  detailsphoto
//    private static final String CREATE_TABLE_VIDEO_DEAILS="CREATE TABLE IF NOT EXISTS "
//            + TABLE_SUBBRAND_VIDEO + "(" + BRAND_ID + " TEXT," +  COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_VIDEO_ID + " TEXT," + COLOUMN_SUBBRAND_VIDEO_CATEGORY + " TEXT," + COLOUMN_SUBBRAND_DETAIL_VIDEOURL + " TEXT " + ")";

    //  detailspack
//    private static final String CREATE_TABLE_PACK_DEAILS="CREATE TABLE IF NOT EXISTS "
//            + TABLE_SUBBRAND_PACK + "(" + BRAND_ID + " TEXT," +  COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_PACK_ID + " TEXT," + COLOUMN_SUBBRAND_PACK_CATEGORY + " TEXT," + COLOUMN_SUBBRAND_DETAIL_PACKURL + " TEXT " + ")";

    private static final String CREATE_TABLE_PACK_DEAILS="CREATE TABLE IF NOT EXISTS "
            + TABLE_SUBBRAND_PACK + "(" +  COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_DETAIL_PACKURL + " TEXT " + ")";

    private static final String CREATE_TABLE_PACK_SUBVIDEO="CREATE TABLE IF NOT EXISTS "
            + TABLE_SUBVIDEO_PACK + "(" +  COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_SUBVIDEO_PACKURL + " TEXT " + ")";

    //  detailsEnga
//    private static final String CREATE_TABLE_ENGA_DEAILS="CREATE TABLE IF NOT EXISTS "
//            + TABLE_SUBBRAND_ENGA + "(" + BRAND_ID + " TEXT," +  COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_SUBBRAND_ENGA_ID + " TEXT," + COLOUMN_SUBBRAND_ENGA_CATEGORY + " TEXT," + COLOUMN_SUBBRAND_DETAIL_ENGAURL + " TEXT " + ")";

    //  timetable
    private static final String CREATE_TABLE_TIME="CREATE TABLE IF NOT EXISTS "
            + TABLE_TIME + "(" + COLOUMN_SUBBRAND_ID + " TEXT," +  USER_ID + " TEXT," + PACK_TIME + " TEXT," + PHOTO_TIME + " TEXT," + VIDEO_TIME + " TEXT, " + ENG_TIME + " TEXT " + ")";

    //  timetable
    private static final String CREATE_TABLE_TIME_FORMAT="CREATE TABLE IF NOT EXISTS "
            + TABLE_TIME_FORMAT + "(" + COLOUMN_SUBBRAND_ID + " TEXT," + USER_ID + " TEXT," + COLOUMN_SUBBRAND_NAME + " TEXT," + PACK_TIME + " TEXT," + PHOTO_TIME + " TEXT," + VIDEO_TIME + " TEXT, " + ENG_TIME + " TEXT " + ")";

    private static final String CREATE_PHOTO_PACK="CREATE TABLE IF NOT EXISTS "
            + TABLE_PHTOT_PACK + "(" + COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_PHTOT_THUBNAIL + " TEXT," + COLOUMN_PHTOT_PACK + " TEXT" + ")";

    private static final String CREATE_VIDEO_PACK="CREATE TABLE IF NOT EXISTS "
            + TABLE_VIDEO_PACK + "(" + COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_VIDEO_THUBNAIL + " TEXT," + COLOUMN_VIDEO_PACK + " TEXT" + ")";

    private static final String CREATE_ENGM_PACK="CREATE TABLE IF NOT EXISTS "
            + TABLE_ENGM_PACK + "(" + COLOUMN_SUBBRAND_ID + " TEXT," + COLOUMN_ENGM_THUBNAIL + " TEXT," + COLOUMN_ENGM_PACK + " TEXT" + ")";

    private static final String CREATE_USER_ID="CREATE TABLE IF NOT EXISTS "
            + TABLE_USER_ID + "(" + USER_ID + " TEXT" + ")";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(CREATE_TABLE_BRAND);
        db.execSQL(CREATE_TABLE_SUBBRAND);
//        db.execSQL(CREATE_TABLE_PHOTO_DEAILS);
//        db.execSQL(CREATE_TABLE_VIDEO_DEAILS);
        db.execSQL(CREATE_TABLE_PACK_DEAILS);
        db.execSQL(CREATE_TABLE_PACK_SUBVIDEO);
        db.execSQL(CREATE_TABLE_TIME);
        db.execSQL(CREATE_TABLE_TIME_FORMAT);
       db.execSQL(CREATE_PHOTO_PACK);
        db.execSQL(CREATE_VIDEO_PACK);
        db.execSQL(CREATE_ENGM_PACK);
        db.execSQL(CREATE_USER_ID);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRAND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBBRAND);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBBRAND_PHOTO);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBBRAND_VIDEO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBBRAND_PACK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIME_FORMAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHTOT_PACK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEO_PACK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENGM_PACK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_ID);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBVIDEO_PACK);

        onCreate(db);
    }

    //userId
    public void insertUserId(USERIDMODELS useridmodels)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_ID, useridmodels.getUserId());
        dbb.insert(TABLE_USER_ID, null , contentValues);
        dbb.close();
    }

    @SuppressLint("Assert")
    public ArrayList<USERIDMODELS> getuserId() {
        ArrayList<USERIDMODELS> datarecord = new ArrayList<USERIDMODELS>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_USER_ID ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                USERIDMODELS dataTask = new USERIDMODELS();
                dataTask.setUserId(cursor.getString(0));
                datarecord.add(dataTask);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }


    // Main Brand
   public void insertData(OfflineDataList dataModels)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BRAND_ID, dataModels.getBrandId());
        contentValues.put(COLOUMN_MAINBRAND_NAME, dataModels.getBrandName());
        contentValues.put(COLOUMN_MAINBRAND_VIDEO, dataModels.getBrandVideo());
        dbb.insert(TABLE_BRAND, null , contentValues);
        dbb.close();
    }
    @SuppressLint("Assert")
    public ArrayList<OfflineDataList> getdataList() {
        ArrayList<OfflineDataList> datarecord = new ArrayList<OfflineDataList>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BRAND ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                OfflineDataList dataTask = new OfflineDataList();
                dataTask.setBrandId(cursor.getString(0));
                dataTask.setBrandName(cursor.getString(1));
                dataTask.setBrandVideo(cursor.getString(2));

                datarecord.add(dataTask);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

// SubBrand TAble
    public void insertSubBrand(OfflineSubBrandResponse subBrandModel)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BRAND_ID, subBrandModel.getBrandId());
        contentValues.put(COLOUMN_SUBBRAND_ID, subBrandModel.getSubBrandId());
        contentValues.put(COLOUMN_SUBBRAND_NAME, subBrandModel.getSubBrandName());
        contentValues.put(COLOUMN_SUBBRAND_PHOTO, subBrandModel.getSubBrandPhoto());
        contentValues.put(COLOUMN_SUBBRAND_PACK_VIDEO, subBrandModel.getPackVideo());
        dbb.insert(TABLE_SUBBRAND, null , contentValues);
        dbb.close();
    }
    @SuppressLint("Assert")
    public ArrayList<OfflineSubBrandResponse> getsubBrandList(String brandId) {
        ArrayList<OfflineSubBrandResponse> datarecord = new ArrayList<OfflineSubBrandResponse>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SUBBRAND + " WHERE brandId = "+brandId ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                OfflineSubBrandResponse dataTask = new OfflineSubBrandResponse();
                dataTask.setBrandId(cursor.getString(0));
                dataTask.setSubBrandId(cursor.getString(1));
                dataTask.setSubBrandName(cursor.getString(2));
                dataTask.setSubBrandPhoto(cursor.getString(3));
                dataTask.setPackVideo(cursor.getString(4));
                datarecord.add(dataTask);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

    // SubBrandPackDetails Table
    public void insertSubBrandDetailsPack(OfflinePack offlinePack)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, offlinePack.getSubBrandId());
        contentValues.put(COLOUMN_SUBBRAND_DETAIL_PACKURL, offlinePack.getFileName());
        dbb.insert(TABLE_SUBBRAND_PACK, null , contentValues);
        dbb.close();
    }

    public void insertPackSubVideo(OfflinePack offlinePack)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, offlinePack.getSubBrandId());
        contentValues.put(COLOUMN_SUBBRAND_SUBVIDEO_PACKURL, offlinePack.getFileName());
        dbb.insert(TABLE_SUBVIDEO_PACK, null , contentValues);
        dbb.close();
    }


    public void insertTime(TimeSpendModelDb timeSpendModelDb)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, timeSpendModelDb.getSubBrandId());
        contentValues.put(USER_ID, timeSpendModelDb.getUserId());
        contentValues.put(PACK_TIME, timeSpendModelDb.getPackTime());
        contentValues.put(PHOTO_TIME, timeSpendModelDb.getPhotoTime());
        contentValues.put(VIDEO_TIME, timeSpendModelDb.getVideoTime());
        contentValues.put(ENG_TIME, timeSpendModelDb.getEngTime());
        dbb.insert(TABLE_TIME, null , contentValues);
        dbb.close();
    }
    @SuppressLint("Assert")
    public TimeSpendModelDb getTImeList(String subBrandId) {
        TimeSpendModelDb dataTask = new TimeSpendModelDb();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_TIME + " WHERE subbrandId = "+subBrandId ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {

                dataTask.setSubBrandId(cursor.getString(0));
                dataTask.setUserId(cursor.getString(1));
                dataTask.setPackTime(cursor.getLong(2));
                dataTask.setPhotoTime(cursor.getLong(3));
                dataTask.setVideoTime(cursor.getLong(4));
                dataTask.setEngTime(cursor.getLong(5));
            }while (cursor.moveToNext());
        }
        return dataTask;
    }
    public void updateTime(TimeSpendModelDb timeSpendModelDb)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, timeSpendModelDb.getSubBrandId());
        contentValues.put(USER_ID, timeSpendModelDb.getUserId());
        contentValues.put(PACK_TIME, timeSpendModelDb.getPackTime());
        contentValues.put(PHOTO_TIME, timeSpendModelDb.getPhotoTime());
        contentValues.put(VIDEO_TIME, timeSpendModelDb.getVideoTime());
        contentValues.put(ENG_TIME, timeSpendModelDb.getEngTime());
        dbb.update(TABLE_TIME,  contentValues,"subbrandId = ? ", new String[] { timeSpendModelDb.getSubBrandId() });
        dbb.close();
    }

    public void insertFormattedTime(FormattedTime timeSpendModelDb)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, timeSpendModelDb.getSubBrandId());
        contentValues.put(USER_ID, timeSpendModelDb.getUserId());
        contentValues.put(COLOUMN_SUBBRAND_NAME, timeSpendModelDb.getSubBrandName());
        contentValues.put(PACK_TIME, timeSpendModelDb.getPackTime());
        contentValues.put(PHOTO_TIME, timeSpendModelDb.getPhotoTime());
        contentValues.put(VIDEO_TIME, timeSpendModelDb.getVideoTime());
        contentValues.put(ENG_TIME, timeSpendModelDb.getEngTime());
        dbb.insert(TABLE_TIME_FORMAT, null , contentValues);
        dbb.close();
    }

    public void updateFormattedTime(FormattedTime timeSpendModelDb)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, timeSpendModelDb.getSubBrandId());
        contentValues.put(USER_ID, timeSpendModelDb.getUserId());
        contentValues.put(COLOUMN_SUBBRAND_NAME, timeSpendModelDb.getSubBrandName());
        contentValues.put(PACK_TIME, timeSpendModelDb.getPackTime());
        contentValues.put(PHOTO_TIME, timeSpendModelDb.getPhotoTime());
        contentValues.put(VIDEO_TIME, timeSpendModelDb.getVideoTime());
        contentValues.put(ENG_TIME, timeSpendModelDb.getEngTime());
        dbb.update(TABLE_TIME_FORMAT,  contentValues,"subbrandId = ? ", new String[] { timeSpendModelDb.getSubBrandId() });
        dbb.close();
    }

    @SuppressLint("Assert")
    public ArrayList<FormattedTime> getFormattedTimeList() {
        ArrayList<FormattedTime> formattedTimeArrayList = new ArrayList<FormattedTime>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_TIME_FORMAT;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                FormattedTime formattedTime = new FormattedTime();
                formattedTime.setSubBrandId(cursor.getString(0));
                formattedTime.setUserId(cursor.getString(1));
                formattedTime.setSubBrandName(cursor.getString(2));
                formattedTime.setPackTime(cursor.getString(3));
                formattedTime.setPhotoTime(cursor.getString(4));
                formattedTime.setVideoTime(cursor.getString(5));
                formattedTime.setEngTime(cursor.getString(6));
                formattedTimeArrayList.add(formattedTime);
            }while (cursor.moveToNext());
        }
        return formattedTimeArrayList;
    }


    @SuppressLint("Assert")
    public ArrayList<OfflinePack> getsubBrandDetailsPackList(String subBrandId) {
        ArrayList<OfflinePack> datarecord = new ArrayList<OfflinePack>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SUBBRAND_PACK + " WHERE subbrandId = "+subBrandId ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                OfflinePack dataTask = new OfflinePack();

                dataTask.setFileName(cursor.getString(1));
                datarecord.add(dataTask);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

    @SuppressLint("Assert")
    public ArrayList<OfflinePack> getPackSubVideoList(String subBrandId) {
        ArrayList<OfflinePack> datarecord = new ArrayList<OfflinePack>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SUBVIDEO_PACK + " WHERE subbrandId = "+subBrandId ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                OfflinePack dataTask = new OfflinePack();

                dataTask.setFileName(cursor.getString(1));
                datarecord.add(dataTask);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

    public void insertPackPhoto(PhotoList photoList)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, photoList.getSubBrandId());
        contentValues.put(COLOUMN_PHTOT_THUBNAIL, photoList.getThumbnail());
        contentValues.put(COLOUMN_PHTOT_PACK, photoList.getFileName());
        dbb.insert(TABLE_PHTOT_PACK, null , contentValues);
        dbb.close();
    }
    @SuppressLint("Assert")
    public ArrayList<PhotoList> getpackPhoto(String subBrandId) {
        ArrayList<PhotoList> datarecord = new ArrayList<PhotoList>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PHTOT_PACK + " WHERE subbrandId = "+subBrandId ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                PhotoList photoList = new PhotoList();
                photoList.setSubBrandId(cursor.getString(0));
                photoList.setThumbnail(cursor.getString(1));
                photoList.setFileName(cursor.getString(2));
                datarecord.add(photoList);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

    public void insertPackVideo(VideoList videoList)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, videoList.getSubBrandId());
        contentValues.put(COLOUMN_VIDEO_THUBNAIL, videoList.getThumbnail());
        contentValues.put(COLOUMN_VIDEO_PACK, videoList.getFileName());
        dbb.insert(TABLE_VIDEO_PACK, null , contentValues);
        dbb.close();
    }
    @SuppressLint("Assert")
    public ArrayList<VideoList> getpackVideo(String subBrandId) {
        ArrayList<VideoList> datarecord = new ArrayList<VideoList>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_VIDEO_PACK + " WHERE subbrandId = "+subBrandId ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                VideoList videoList = new VideoList();
                videoList.setSubBrandId(cursor.getString(0));
                videoList.setThumbnail(cursor.getString(1));
                videoList.setFileName(cursor.getString(2));
                datarecord.add(videoList);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

    public void insertPackEngm(EngagementList engagementList)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLOUMN_SUBBRAND_ID, engagementList.getSubBrandId());
        contentValues.put(COLOUMN_ENGM_THUBNAIL, engagementList.getThumbnail());
        contentValues.put(COLOUMN_ENGM_PACK, engagementList.getFileName());
        dbb.insert(TABLE_ENGM_PACK, null , contentValues);
        dbb.close();
    }
    @SuppressLint("Assert")
    public ArrayList<EngagementList> getpackEngm() {
        ArrayList<EngagementList> datarecord = new ArrayList<EngagementList>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ENGM_PACK  ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                EngagementList engagementList = new EngagementList();
                engagementList.setSubBrandId(cursor.getString(0));
                engagementList.setThumbnail(cursor.getString(1));
                engagementList.setFileName(cursor.getString(2));
                datarecord.add(engagementList);
            }while (cursor.moveToNext());
        }
        return datarecord;
    }

    // Db Clear

    public void clearAll()
    {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(DBHelper.TABLE_BRAND,null,null);
        db.delete(DBHelper.TABLE_SUBBRAND,null,null);
        db.delete(DBHelper.TABLE_SUBBRAND_PACK,null,null);
        db.delete(DBHelper.TABLE_SUBVIDEO_PACK,null,null);
        db.delete(DBHelper.TABLE_TIME,null,null);
        db.delete(DBHelper.TABLE_TIME_FORMAT,null,null);
        db.delete(DBHelper.TABLE_PHTOT_PACK,null,null);
        db.delete(DBHelper.TABLE_VIDEO_PACK,null,null);
        db.delete(DBHelper.TABLE_ENGM_PACK,null,null);

    }

    // For testing----------------------------
    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"mesage"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            //     DeBug.showLog("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {

            ////      DeBug.showLog("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }


}
