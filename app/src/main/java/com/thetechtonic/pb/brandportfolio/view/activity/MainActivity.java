package com.thetechtonic.pb.brandportfolio.view.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.data.DataManager;
import com.thetechtonic.pb.brandportfolio.data.SessionManager;
import com.thetechtonic.pb.brandportfolio.data.event.BrandListSuccessEvent;
import com.thetechtonic.pb.brandportfolio.data.event.ErrorEvent;
import com.thetechtonic.pb.brandportfolio.data.event.OfflineSuccessEvent;
import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataList;
import com.thetechtonic.pb.brandportfolio.utils.CheckForSDCard;
import com.thetechtonic.pb.brandportfolio.utils.CustomViewPager;
import com.thetechtonic.pb.brandportfolio.utils.Utils;
import com.thetechtonic.pb.brandportfolio.utils.constants.S;
import com.thetechtonic.pb.brandportfolio.view.adapter.MyPagerAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity {

    private static CustomViewPager viewPager;
    EventBus event;
    String userId;
    SharedPreferences sharedPreferences;
    private static ProgressDialog progressDialog;
    private ProgressDialog mProgressDialog0;
    MyPagerAdapter adapterViewPager;
    List<BrandListResponse.BrandList> offlineDataLists;
    List<OfflineDataList> brandList = new ArrayList<>();
    private static String downloadFileName = "";
    private DBHelper dbHelper;
    private List<OfflineDataList> dataLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        event = EventBus.getDefault();
        sharedPreferences = getSharedPreferences(S.MyPREFERENCES, Context.MODE_PRIVATE);
        userId = sharedPreferences.getString(S.ID, "");
        dbHelper = new DBHelper(this);
        progressDialog = new ProgressDialog(this);

        // Initialize the progress dialog
        mProgressDialog0 = new ProgressDialog(this);
        mProgressDialog0.setIndeterminate(false);
        mProgressDialog0.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog0.setTitle("Downloading Brand Videos");
        mProgressDialog0.setMessage("Please wait, we are downloading your files...");
        mProgressDialog0.setCancelable(false);

        viewPager = findViewById(R.id.viewPager);
        if (Utils.isConnectedToInternet(this)) {
            dataLists = dbHelper.getdataList();
            if (dataLists.isEmpty()){
            callOfflinedata();
            }else {
                adapterViewPager = new MyPagerAdapter(MainActivity.this,getSupportFragmentManager(),dataLists,dataLists.size());
                viewPager.setAdapter(adapterViewPager);
            }
        } else {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton("OK", (dialog, which) -> {
                    alertDialog.dismiss();
                    finish();
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.d("TAG", "Show Dialog: " + e.getMessage());
            }
        }

    }


    public void callOfflinedata() {
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        showLoading("Please wait...");
        RequestBody deviceIdBody = RequestBody.create(MediaType.parse("text/plain"), android_id);
        DataManager.getBranList();
    }

    @Subscribe
    public void onSuccess(BrandListSuccessEvent event) {
        hideLoading();
        offlineDataLists = new ArrayList<>();
        offlineDataLists = event.getBrandList();
        for (int i= 0;i<offlineDataLists.size();i++){
            String brandVideoUrl = offlineDataLists.get(i).getBrandVideo();
            String path = brandVideoUrl.replace(Utils.mainUrl, "");
            String basePath = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory;
            new BrandVideoDownloadingTask().execute(offlineDataLists.get(i).getBrandVideo());
            dbHelper.insertData(new OfflineDataList(offlineDataLists.get(i).getBrandId(),offlineDataLists.get(i).getBrandName(),
                    basePath+"/"+path));
        }

    }


    @Subscribe
    public void onFailed(ErrorEvent event) {
        hideLoading();
        Toast.makeText(this, event.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onStart() {
        super.onStart();
        event.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        event.unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    public static void showLoading (String message)
    {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void hideLoading ()
    {
        if (progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    private  class BrandVideoDownloadingTask extends AsyncTask<String, Integer, String> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog0.setMax(offlineDataLists.size());
            mProgressDialog0.show();
            //mProgressDialog.setProgress(0);
        }

        @Override
        protected String doInBackground(String... arg0) {
            // download and publish progress
            int imagesCount = offlineDataLists.size();
            for (int i=0;i<offlineDataLists.size();i++) {
                try {

                    downloadFileName = arg0[0].replace(Utils.mainUrl, "");
                    URL url = new URL(arg0[0]);//Create Download URl
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                    c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                    c.connect();//connect the URL Connection
                    //lenghtOfFile is used for calculating download progress
                    int lenghtOfFile = c.getContentLength();
                    //If Connection response is not OK then show Logs
                    if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.e("TAG", "Server returned HTTP " + c.getResponseCode()
                                + " " + c.getResponseMessage());

                    }


                    //Get File if SD card is present
                    if (new CheckForSDCard().isSDCardPresent()) {

                        apkStorage = new File(
                                Environment.getExternalStorageDirectory() + "/"
                                        + Utils.downloadDirectory);
                    } else
                        Toast.makeText(MainActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                    //If File is not present create directory
                    if (!apkStorage.exists()) {
                        apkStorage.mkdir();
                        Log.e("TAG", "Directory Created.");
                    }

                    outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                    //Create New File if not present
                    if (!outputFile.exists()) {
                        outputFile.createNewFile();
                        Log.e("TAG", "File Created");
                    }

                    FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                    InputStream is = c.getInputStream();//Get InputStream for connection

                    byte[] buffer = new byte[1024];//Set buffer type
                    int len1 = 0;//init length
                    long total = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        total += len1;

                        fos.write(buffer, 0, len1);//Write new file
                    }
                    publishProgress();
                    //Close all connection after doing task
                    fos.close();
                    is.close();

                } catch (Exception e) {

                    //Read exception if something went wrong
                    e.printStackTrace();
                    outputFile = null;
                    Log.e("TAG", "Download Error Exception " + e.getMessage());
                }
            }
            return null;
        }

        // After each task done
        protected void onProgressUpdate(Integer... progress){
        // Display the progress on text view

        //mProgressDialog.setProgress(progress[0]);
            //mProgressDialog.incrementProgressBy(1);
            super.onProgressUpdate();
        }

        @Override
        protected void onPostExecute(String result) {
            offlineDataLists.remove(0);
            if (offlineDataLists.isEmpty()){
                // Hide the progress dialog
                mProgressDialog0.dismiss();

                String path = Environment.getExternalStorageDirectory() + "/" + Utils.downloadDirectory;
                Log.d("Files", "Path: " + path);
                File directory = new File(path);
                File[] files = directory.listFiles();
                for (int i = 0; i < files.length; i++)
                {
                    OfflineDataList brandListData = new OfflineDataList();
                    brandListData.setBrandId(String.valueOf(i+1));
                    brandListData.setBrandVideo(path+"/"+files[i].getName());
                    brandListData.setBrandName(files[i].getName());
                    brandList.add(brandListData);
                    Log.d("Files", "FileName:" + files[i].getName());
                }
                adapterViewPager = new MyPagerAdapter(MainActivity.this,getSupportFragmentManager(),brandList,brandList.size());
                viewPager.setAdapter(adapterViewPager);

            }else {
                mProgressDialog0.incrementProgressBy(1);
                //Toast.makeText(MainActivity.this,"Complete",Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }
    }




}
