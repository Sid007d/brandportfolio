package com.thetechtonic.pb.brandportfolio.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EngagementList implements Parcelable{

    @SerializedName("subBrandDetailId")
    @Expose
    private String subBrandDetailId;
    @SerializedName("subBrandId")
    @Expose
    private String subBrandId;
    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("fileName")
    @Expose
    private String fileName;

    public EngagementList(Parcel in) {
        subBrandDetailId = in.readString();
        subBrandId = in.readString();
        brandId = in.readString();
        category = in.readString();
        thumbnail = in.readString();
        fileName = in.readString();
    }

    public static final Creator<EngagementList> CREATOR = new Creator<EngagementList>() {
        @Override
        public EngagementList createFromParcel(Parcel in) {
            return new EngagementList(in);
        }

        @Override
        public EngagementList[] newArray(int size) {
            return new EngagementList[size];
        }
    };

    public EngagementList() {

    }

    public String getSubBrandDetailId() {
        return subBrandDetailId;
    }

    public void setSubBrandDetailId(String subBrandDetailId) {
        this.subBrandDetailId = subBrandDetailId;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(subBrandDetailId);
        parcel.writeString(subBrandId);
        parcel.writeString(brandId);
        parcel.writeString(category);
        parcel.writeString(thumbnail);
        parcel.writeString(fileName);
    }
}
