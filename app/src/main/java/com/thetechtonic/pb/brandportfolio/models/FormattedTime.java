package com.thetechtonic.pb.brandportfolio.models;

public class FormattedTime {

    String subBrandId;
    String subBrandName;
    String packTime;
    String photoTime;
    String videoTime;
    String engTime;
    String userId;

    public FormattedTime(String subBrandId,String userId,String subBrandName, String packTime, String photoTime, String videoTime, String engTime) {
        this.subBrandId = subBrandId;
        this.packTime = packTime;
        this.photoTime = photoTime;
        this.videoTime = videoTime;
        this.engTime = engTime;
        this.subBrandName = subBrandName;
        this.userId=userId;
    }

    public FormattedTime() {
    }

    public String getSubBrandName() {
        return subBrandName;
    }

    public void setSubBrandName(String subBrandName) {
        this.subBrandName = subBrandName;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getPackTime() {
        return packTime;
    }

    public void setPackTime(String packTime) {
        this.packTime = packTime;
    }

    public String getPhotoTime() {
        return photoTime;
    }

    public void setPhotoTime(String photoTime) {
        this.photoTime = photoTime;
    }

    public String getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(String videoTime) {
        this.videoTime = videoTime;
    }

    public String getEngTime() {
        return engTime;
    }

    public void setEngTime(String engTime) {
        this.engTime = engTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
