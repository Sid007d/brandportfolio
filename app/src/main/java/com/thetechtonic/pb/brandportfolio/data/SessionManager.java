package com.thetechtonic.pb.brandportfolio.data;

/**
 * Created by Sid on 03-06-2018.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.HashMap;
import java.util.List;


public class SessionManager {
    // Shared Preferences
    public static SharedPreferences pref;

    // Editor for Shared preferences
    public static Editor editor;
    public static ProgressDialog progressDialog;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "PlayBoxPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String VIDEO_URL = "videoUrl";
    public static final String IS_SAVED = "isSaved";
    public static final HashMap<String, String> hmap = new HashMap<String, String>();

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        progressDialog = new ProgressDialog(_context);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        /*// Storing name in pref
        editor.putString(KEY_ID, id);

        // Storing email in pref
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_AMOUNT,amount);*/
        // commit changes
        editor.commit();
    }

    public static void savePreferences(String videoUrl,String isSaved){
        editor.putString(VIDEO_URL,videoUrl);
        editor.putString(IS_SAVED,isSaved);
        hmap.put(videoUrl,isSaved);
        editor.commit();
    }

    public static String readPreferences(String videoUrl,String isSaved){
       String savedValue;
       savedValue= hmap.get(videoUrl);


       return savedValue;
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    /*public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, AuthActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }else{
            Intent i = new Intent(_context, HomeActivity.class);
            _context.startActivity(i);
        }

    }*/



    /**
     * Clear session details
     * */
    /*public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Fragment
        Intent i = new Intent(_context, AuthActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }*/

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public static void showLoading (String message)
    {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void hideLoading ()
    {
        if (progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }
}
