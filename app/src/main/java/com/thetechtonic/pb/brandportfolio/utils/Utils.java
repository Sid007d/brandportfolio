package com.thetechtonic.pb.brandportfolio.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by SONU on 29/10/15.
 */
public class Utils {

    //String Values to be Used in App
    public static final String downloadDirectory = "BRAND PORTFOLIO";
    public static final String downloadSubDirectory = "Pack Videos";
    public static final String downloadSubVideos = "Pack Sub Videos";
    public static final String mainUrl = "http://propcharge.com/PORTFOLIO_APP/portfolio_app/uploads/brands/";
//    public static final String subUrl="http://propcharge.com/PORTFOLIO_APP/portfolio_app/uploads/subBrands/pack/";
    public static final String subBrandsUrl="http://propcharge.com/PORTFOLIO_APP/portfolio_app/uploads/subBrands/";
    public static final String packUrl = "http://propcharge.com/PORTFOLIO_APP/portfolio_app/uploads/subBrands/pack/";
    public static final String packPhotoUrl = "http://propcharge.com/PORTFOLIO_APP/portfolio_app/uploads/subBrands/photo/";
    public static final String subVideoUrl = "http://propcharge.com/PORTFOLIO_APP/portfolio_app/uploads/subBrands/video/";

    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager connectivity = (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }

}
