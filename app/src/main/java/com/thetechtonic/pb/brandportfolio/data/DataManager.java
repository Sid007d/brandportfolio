package com.thetechtonic.pb.brandportfolio.data;



import com.thetechtonic.pb.brandportfolio.data.remote.AuthAPI;
import com.thetechtonic.pb.brandportfolio.models.RawUserData;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Sid on 03-06-2018.
 */

public class DataManager
{
    private static AuthAPI sAuthAPI = new AuthAPI();
    public static String getUserToken ()
    {
        return "";
    }


  //BRAND LIST
    public static void getBranList()
    {
        sAuthAPI.getBranList();
    }

    //getSubBranList
    public static void getSubBranList(RequestBody brandId)
    {
        sAuthAPI.getSubBranList(brandId);
    }
   //SubBrandDetails
    public static void getSubBradDetails(RequestBody brandId)
    {
        sAuthAPI.getSubBradDetails(brandId);
    }
    //getofflinedata   callTime
    public static void callTime(RequestBody customerId, RequestBody customerName,RequestBody brandId,RequestBody subBrandPackTime,RequestBody subBrandPhotoTime,RequestBody subBrandVideoTime,RequestBody subBrandEngagementTime)
    {
        sAuthAPI.callTime(customerId,customerName,brandId,subBrandPackTime,subBrandPhotoTime,subBrandVideoTime,subBrandEngagementTime);
    }

    //   getofflinedata
    public static void getofflinedata(RequestBody deviceId)
    {
        sAuthAPI.getofflinedata(deviceId);
    }
    //   getofflinedata
    public static void getofflinedataV2(RequestBody deviceId)
    {
        sAuthAPI.getofflinedataV2(deviceId);
    }

    public static void submitRawData(RawUserData rawUserData)
    {
        sAuthAPI.submitRawData(rawUserData);
    }
}
