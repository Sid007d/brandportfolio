package com.thetechtonic.pb.brandportfolio.view.interfaces;

/**
 * Created by Sid on 28-08-2018.
 */

public interface FragmentLifecycle {

    public void onPauseFragment();
    public void onResumeFragment();
}
