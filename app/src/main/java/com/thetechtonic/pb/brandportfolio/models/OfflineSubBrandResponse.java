package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfflineSubBrandResponse {

    @SerializedName("brandId")
    @Expose
    private String brandId;
    @SerializedName("subBrandId")
    @Expose
    private String subBrandId;
    @SerializedName("subBrandName")
    @Expose
    private String subBrandName;
    @SerializedName("variant_id")
    @Expose
    private String variant_id;
    @SerializedName("subBrandPhoto")
    @Expose
    private String subBrandPhoto;
    @SerializedName("pack")
    @Expose
    private OfflinePack pack;
    @SerializedName("photo")
    @Expose
    private List<OfflinePhotoList> photo = null;
    @SerializedName("video")
    @Expose
    private List<OfflineVideoList> video = null;
    @SerializedName("engagement")
    @Expose
    private List<OfflineEngList> engagement = null;

    private String packVideo;

    public OfflineSubBrandResponse(String brandId, String subBrandId, String subBrandName,  String variant_id,String subBrandPhoto) {
        this.brandId = brandId;
        this.subBrandId = subBrandId;
        this.subBrandName = subBrandName;
        this.variant_id = variant_id;
        this.subBrandPhoto = subBrandPhoto;

    }

    public OfflineSubBrandResponse() {
    }

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public String getPackVideo() {
        return packVideo;
    }

    public void setPackVideo(String packVideo) {
        this.packVideo = packVideo;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getSubBrandName() {
        return subBrandName;
    }

    public void setSubBrandName(String subBrandName) {
        this.subBrandName = subBrandName;
    }

    public String getSubBrandPhoto() {
        return subBrandPhoto;
    }

    public void setSubBrandPhoto(String subBrandPhoto) {
        this.subBrandPhoto = subBrandPhoto;
    }

    public OfflinePack getPack() {
        return pack;
    }

    public void setPack(OfflinePack pack) {
        this.pack = pack;
    }

    public List<OfflinePhotoList> getPhoto() {
        return photo;
    }

    public void setPhoto(List<OfflinePhotoList> photo) {
        this.photo = photo;
    }

    public List<OfflineVideoList> getVideo() {
        return video;
    }

    public void setVideo(List<OfflineVideoList> video) {
        this.video = video;
    }

    public List<OfflineEngList> getEngagement() {
        return engagement;
    }

    public void setEngagement(List<OfflineEngList> engagement) {
        this.engagement = engagement;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }
}
