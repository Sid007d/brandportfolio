package com.thetechtonic.pb.brandportfolio.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.models.VideoList;
import com.thetechtonic.pb.brandportfolio.view.activity.VideoPlayActivity;
import java.util.List;

/**
 * Created by admin on 8/29/2018.
 */

public class VideoItemAdapter extends RecyclerView.Adapter<VideoItemAdapter.SingleItemRowHolder> {

    private List<VideoList> itemsList;
    private Context mContext;

    public VideoItemAdapter(Context context, List<VideoList> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public SingleItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.videochild, viewGroup, false);
        return new SingleItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleItemRowHolder holder, int i) {

        VideoList singleItem = itemsList.get(i);
        holder.tvTitle.setText(singleItem.getCaption());

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(singleItem.getFileName(), MediaStore.Video.Thumbnails.MINI_KIND);
        holder.itemImage.setImageBitmap(thumb);

        /*Glide.with(mContext)
                .load(singleItem.getThumbnail())
                .into(holder.itemImage);*/

        holder.llVideoPlay.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, VideoPlayActivity.class);
            intent.putExtra("videoUrl",singleItem.getFileName());
            intent.putExtra("subBrandId",singleItem.getSubBrandId());
            mContext.startActivity(intent);
        });
    }



    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;

        protected LinearLayout llVideoPlay;

         SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle =  view.findViewById(R.id.tvTitle);
            this.itemImage =  view.findViewById(R.id.itemImage);
            this.llVideoPlay = view.findViewById(R.id.llVideoPlay);


            view.setOnClickListener(v -> Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show());


        }

    }

}
