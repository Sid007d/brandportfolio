package com.thetechtonic.pb.brandportfolio.utils.constants;

/**
 * Created by Sid on 03-06-2018.
 */

public class S
{
    //TITLES
    public static String title_masuk         = "Masuk";
    public static String title_lupa_password = "Lupa Password?";

    public static String loading_text = "Loading";
    public static String loading_auth = "Authenticating...";

    //SUCCESS MESSAGES
    public static String success_lupa_password = "Please check your email.";

    // ERROR MESSAGES
    public static String error_generic               = "%s Failed";
    public static String error_id_kosong             = "Field cannot be empty!";
    public static String error_lupa_password         = "Reset Password Failed";
    public static String error_password_kosong       = "Password cannot be empty!";
    public static String error_tidak_ada_data        = "No Data";
    public static String error_unknown               = "Unknown error. Please try again.";
    public static String error_connect               = "Connection error. Please try again";
    public static String camera_permission_message   = "Access to camera is needed";
    public static String location_permission_message = "Access to location service is needed";
    //Email Validation pattern
    public static final String regEx = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    //Fragments Tags
    public static final String Login_Fragment = "Login_Fragment";
    public static final String SignUp_Fragment = "SignUp_Fragment";
    public static final String ForgotPassword_Fragment = "ForgotPassword_Fragment";

    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME = "com.thetechtonic.pb.playbox";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    public static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
    public static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
    public static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";

    public static final String LATITUDE_VALUE = "lat" ;
    public static final String LONGITUDE_VALUE = "long" ;
    public static final String ADDRESS = "add" ;
    public static final String USER_NAME = "userName" ;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String PACK_TIME = "pack_time";

    public static final String ISUSERLOGGEDIN= "is_user_logged_in";
    public static final String USER_ID = "user_id";
    public static final String ID = "amount";
    public static final String OTP = "otp";
    public static final String activityID = "activityID";



}
