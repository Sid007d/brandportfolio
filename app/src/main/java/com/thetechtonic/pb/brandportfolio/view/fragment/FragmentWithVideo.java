package com.thetechtonic.pb.brandportfolio.view.fragment;


import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.view.activity.VariantActivity;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentWithVideo extends Fragment {

    private Uri videoUri;
    VideoView videoView;
    private ImageView ivBrandImage;
    private MediaPlayer mediaPlayer;
    private RelativeLayout container;
    private String brandId;
    private ImageView ivLeft,ivRight;

    public static FragmentWithVideo newInstance(String brandName,String video,String brandId) {
        FragmentWithVideo fragment = new FragmentWithVideo();
        Bundle args = new Bundle();
        args.putString("video", video);
        args.putString("brandName",brandName);
        args.putString("brandId",brandId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_with_video, container, false);
        Bundle extras = getArguments();

        videoView = view.findViewById(R.id.videoViewFrag);
        ivLeft = view.findViewById(R.id.ivLeft);
        ivBrandImage = view.findViewById(R.id.ivBrandImage);
        ivRight = view.findViewById(R.id.ivRight);
        container = view.findViewById(R.id.container);

        //tvBrandName.setText(Objects.requireNonNull(extras).getString("brandName"));
        brandId = Objects.requireNonNull(extras).getString("brandId");

        if (Objects.requireNonNull(brandId).equals("1")){
            ivBrandImage.setImageResource(R.mipmap.classic);
        } else if (brandId.equals("2")){
            ivBrandImage.setImageResource(R.mipmap.goldflake);
        } else if (brandId.equals("3")){
            ivBrandImage.setImageResource(R.mipmap.hollywood);
        }else {
            ivBrandImage.setImageResource(R.mipmap.american);
        }

        if (Objects.requireNonNull(brandId).equals("1")){
//            ivLeft.setVisibility(View.GONE);
        }else if (brandId.equals("4")){
            //           ivRight.setVisibility(View.GONE);
        }else {
//            ivRight.setVisibility(View.VISIBLE);
//            ivLeft.setVisibility(View.VISIBLE);
        }
        Uri uri = Uri.parse(Objects.requireNonNull(extras).getString("video"));

        videoView.setVideoPath(Objects.requireNonNull(extras).getString("video"));
        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer =mp;
                mp.setLooping(true);
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("API123", "What " + what + " extra " + extra);
                return false;
            }
        });
        videoView.start();
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),VariantActivity.class);
                intent.putExtra("brandId",brandId);
                startActivity(intent);
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        videoView.start();
    }

}
