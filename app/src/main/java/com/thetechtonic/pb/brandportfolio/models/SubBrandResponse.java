package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubBrandResponse {

    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("data")
    @Expose
    private List<SubBrandList> data = null;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<SubBrandList> getData() {
        return data;
    }

    public void setData(List<SubBrandList> data) {
        this.data = data;
    }
    public class SubBrandList {

        @SerializedName("subBrandId")
        @Expose
        private String subBrandId;
        @SerializedName("subBrandName")
        @Expose
        private String subBrandName;
        @SerializedName("variant_id")
        @Expose
        private String variant_id;
        @SerializedName("subBrandPhoto")
        @Expose
        private String subBrandPhoto;

        public String getVariant_id() {
            return variant_id;
        }

        public void setVariant_id(String variant_id) {
            this.variant_id = variant_id;
        }

        public String getSubBrandId() {
            return subBrandId;
        }

        public void setSubBrandId(String subBrandId) {
            this.subBrandId = subBrandId;
        }

        public String getSubBrandName() {
            return subBrandName;
        }

        public void setSubBrandName(String subBrandName) {
            this.subBrandName = subBrandName;
        }

        public String getSubBrandPhoto() {
            return subBrandPhoto;
        }

        public void setSubBrandPhoto(String subBrandPhoto) {
            this.subBrandPhoto = subBrandPhoto;
        }

    }
}
