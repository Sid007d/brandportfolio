package com.thetechtonic.pb.brandportfolio.data.event;

import com.thetechtonic.pb.brandportfolio.models.OfflineDataList;
import com.thetechtonic.pb.brandportfolio.models.OfflineDataResponse;


import java.util.ArrayList;
import java.util.List;

public class OfflineV2SuccessEvent extends BaseEvent {

    private String message,code;
    private String flagTime,deciceId;
    private List<OfflineDataList> offlineDataLists=new ArrayList<>();

    public OfflineV2SuccessEvent(OfflineDataResponse offlineDataResponse) {

        message=offlineDataResponse.getResponseMessage();
        code=offlineDataResponse.getResponseCode();
        flagTime=offlineDataResponse.getFlagTime();
        deciceId=offlineDataResponse.getDeviceId();
        offlineDataLists=offlineDataResponse.getData();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFlagTime() {
        return flagTime;
    }

    public void setFlagTime(String flagTime) {
        this.flagTime = flagTime;
    }

    public String getDeciceId() {
        return deciceId;
    }

    public void setDeciceId(String deciceId) {
        this.deciceId = deciceId;
    }

    public List<OfflineDataList> getOfflineDataLists() {
        return offlineDataLists;
    }

    public void setOfflineDataLists(List<OfflineDataList> offlineDataLists) {
        this.offlineDataLists = offlineDataLists;
    }
}
