package com.thetechtonic.pb.brandportfolio.data.event;

import com.thetechtonic.pb.brandportfolio.models.BrandListResponse;

import java.util.ArrayList;
import java.util.List;

public class BrandListSuccessEvent extends BaseEvent {


    String message;
    String responseCode;
    List<BrandListResponse.BrandList> brandList=new ArrayList<>();

    public BrandListSuccessEvent(BrandListResponse brandListResponse) {

        message=brandListResponse.getResponseMessage();
        responseCode=brandListResponse.getResponseCode();
        brandList=brandListResponse.getData();

    }

    public List<BrandListResponse.BrandList> getBrandList() {
        return brandList;
    }

    public String getMessage() {
        return message;
    }

    public String getResponseCode() {
        return responseCode;
    }
}
