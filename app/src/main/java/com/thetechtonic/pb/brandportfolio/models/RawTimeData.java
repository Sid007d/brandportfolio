package com.thetechtonic.pb.brandportfolio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RawTimeData {

    @SerializedName("subBrandId")
    @Expose
    private String subBrandId;
    @SerializedName("packTime")
    @Expose
    private String packTime;
    @SerializedName("photoTime")
    @Expose
    private String photoTime;
    @SerializedName("videoTime")
    @Expose
    private String videoTime;
    @SerializedName("engagementTime")
    @Expose
    private String engagementTime;

    public RawTimeData(String subBrandId, String packTime, String photoTime, String videoTime, String engagementTime) {
        this.subBrandId = subBrandId;
        this.packTime = packTime;
        this.photoTime = photoTime;
        this.videoTime = videoTime;
        this.engagementTime = engagementTime;
    }

    public String getSubBrandId() {
        return subBrandId;
    }

    public void setSubBrandId(String subBrandId) {
        this.subBrandId = subBrandId;
    }

    public String getPackTime() {
        return packTime;
    }

    public void setPackTime(String packTime) {
        this.packTime = packTime;
    }

    public String getPhotoTime() {
        return photoTime;
    }

    public void setPhotoTime(String photoTime) {
        this.photoTime = photoTime;
    }

    public String getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(String videoTime) {
        this.videoTime = videoTime;
    }

    public String getEngagementTime() {
        return engagementTime;
    }

    public void setEngagementTime(String engagementTime) {
        this.engagementTime = engagementTime;
    }
}
