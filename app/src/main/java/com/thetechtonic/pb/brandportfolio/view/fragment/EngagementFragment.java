package com.thetechtonic.pb.brandportfolio.view.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thetechtonic.pb.brandportfolio.R;
import com.thetechtonic.pb.brandportfolio.data.DBHelper;
import com.thetechtonic.pb.brandportfolio.models.EngagementList;
import com.thetechtonic.pb.brandportfolio.models.TimeSpendModelDb;
import com.thetechtonic.pb.brandportfolio.utils.SharedPreferenceManager;
import com.thetechtonic.pb.brandportfolio.view.adapter.EngagmentAdapter;
import com.thetechtonic.pb.brandportfolio.view.adapter.PhotoItemAdapter;
import com.thetechtonic.pb.brandportfolio.view.interfaces.FragmentLifecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.thetechtonic.pb.brandportfolio.view.fragment.PackFragment.msToString;

/**
 * A simple {@link Fragment} subclass.
 */
public class EngagementFragment extends Fragment {

    long startTime;
    long endTime;
    SharedPreferences sharedPreferences;
    private long totalTime;
    private SharedPreferences.Editor editor;
    RecyclerView rcEngagment;
    List<EngagementList> engagementLists=new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private TextView tvNoData;
    private String isData;
    EngagmentAdapter engagmentAdapter;
    DBHelper dbHelper;
    private String subBrandId,variantId;

    public EngagementFragment() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_engagement, container, false);
        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        dbHelper = new DBHelper(getActivity());
        initUI(view);
        inEvent();
        return view;
    }

    public void initUI(View view)
    {
        rcEngagment=view.findViewById(R.id.rcEngagment);
        tvNoData = view.findViewById(R.id.tvNoData);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void inEvent()
    {
        Bundle bundle=getArguments();
        isData = Objects.requireNonNull(bundle).getString("isData");
        if (Objects.requireNonNull(isData).equals("N")){
            tvNoData.setVisibility(View.VISIBLE);

        } else {
            tvNoData.setVisibility(View.GONE);
            engagementLists = Objects.requireNonNull(bundle).getParcelableArrayList("engageList");
            subBrandId = Objects.requireNonNull(bundle).getString("subBrandId");
            variantId = Objects.requireNonNull(bundle).getString("variantId");
            engagmentAdapter= new EngagmentAdapter(getActivity(), engagementLists);
            layoutManager= new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
            rcEngagment.setLayoutManager(layoutManager);
            rcEngagment.setItemAnimator(new DefaultItemAnimator());
            rcEngagment.setAdapter(engagmentAdapter);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onResume() {
        super.onResume();
        startTime = System.currentTimeMillis();
        Bundle bundle=getArguments();
        isData = Objects.requireNonNull(bundle).getString("isData");
        if (Objects.requireNonNull(isData).equals("N")){
            tvNoData.setVisibility(View.VISIBLE);

        } else {
            tvNoData.setVisibility(View.GONE);
            engagementLists = Objects.requireNonNull(bundle).getParcelableArrayList("engageList");
            subBrandId = Objects.requireNonNull(bundle).getString("subBrandId");
            engagmentAdapter= new EngagmentAdapter(getActivity(), engagementLists);
            layoutManager= new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
            rcEngagment.setLayoutManager(layoutManager);
            rcEngagment.setItemAnimator(new DefaultItemAnimator());
            rcEngagment.setAdapter(engagmentAdapter);
        }
    }


    public static String msToString(long ms) {
        long totalSecs = ms/1000;
        long hours = (totalSecs / 3600);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00"
                : ((mins < 10)
                ? "0" + mins
                : "" + mins);
        String secsString = (secs == 0)
                ? "00"
                : ((secs < 10)
                ? "0" + secs
                : "" + secs);
        if (hours > 0)
            return hours+" "+"hr" + ":" + minsString+" "+"min" + ":" + secsString+" "+"sec";
        else if (mins > 0)
            return mins+" "+"min" + ":" + secsString+" "+"sec";
        else return secsString+" "+"sec";
    }

    @Override
    public void onPause() {
        super.onPause();
        TimeSpendModelDb timeSpendModelDbs;
        timeSpendModelDbs = dbHelper.getTImeList(subBrandId);
        totalTime = timeSpendModelDbs.getEngTime();
        endTime = System.currentTimeMillis();
        long timeSpend = endTime - startTime;
        SharedPreferenceManager.setEngTime(getActivity(), timeSpend+totalTime);
        dbHelper.updateTime(new TimeSpendModelDb(variantId, "1",timeSpendModelDbs.getPackTime(), timeSpendModelDbs.getPhotoTime(),timeSpendModelDbs.getVideoTime(),timeSpend+totalTime));
//        editor = sharedPreferences.edit();
//        editor.putLong("DetailScreen4Time", timeSpend+totalTime).apply(); // Storing long
        Log.d("Fragment 4 Time:", msToString(timeSpend+totalTime));
        //Toast.makeText(getActivity(),  "Engage: "+msToString(timeSpend+totalTime), Toast.LENGTH_SHORT).show();
    }
}
